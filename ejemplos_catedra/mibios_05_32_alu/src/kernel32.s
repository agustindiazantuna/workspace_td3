SECTION  .kernel32 progbits
GLOBAL kernel32_init

USE32
kernel32_init:
   
   .kybrd_poll:
      xor eax, eax
      in al, 0x64
      bt eax, 0x00
      jnz .kybrd_poll

      in al, 0x60
      bt eax, 0x07
      jc .kybrd_poll
   
   jmp .kybrd_poll
   .guard:
      hlt
      jmp .guard

