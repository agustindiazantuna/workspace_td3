SECTION	.sys_tables progbits alloc noexec nowrite
GLOBAL CS_SEL_32
GLOBAL DS_SEL
GLOBAL GDT_LENGTH
GLOBAL rom_gdtr

GDT:
NULL_SEL    equ $-GDT
   dq 0x0
CS_SEL_32   equ $-GDT
   dw 0xffff 
   dw 0x0000
   db 0x00
	db 0x99
   db 0xcf
   db 0
DS_SEL      equ $-GDT
   dw 0xffff 
   dw 0x0000
   db 0x00
   db 0x92
   db 0xcf
   db 0
GDT_LENGTH  equ $-GDT

rom_gdtr:
   dw GDT_LENGTH-1
   dd GDT

