USE16
EXTERN start16

SECTION .resetvec
reset_vector:
	cli
	cld
	jmp start16
;align 16      ;Relleno de nop (0x90) resuelto en el enlazador
