%include "./inc/processor-flags.h"

USE16

SECTION  .start16 progbits
GLOBAL   start16
EXTERN   late_board_init
EXTERN   early_board_init
GLOBAL   ..@early_board_init_return
EXTERN   rom_gdtr
EXTERN   CS_SEL_32

start16:
   test  eax, 0x0             ;Verificar que el uP no este en fallo
   jne   .fault_end

   xor   eax, eax
   mov   cr3, eax             ;Invalidar TLB

   jmp   early_board_init        ;Callout para agregar funciones de inicializacion
   ..@early_board_init_return:   ;del procesador y controladores escenciales 

   ;->Deshabilitar cache<-
   mov   eax, cr0
   or    eax, (X86_CR0_NW | X86_CR0_CD)
   mov   cr0, eax
   wbinvd

   o32 lgdt  [cs:rom_gdtr]
   
   call late_board_init      ;Callout para agregar funciones de inicializacion
                              ;de los chipset de la placa

   ;->Establecer el up en MP<-
   smsw  ax
   or	   ax, X86_CR0_PE
   lmsw  ax

   jmp   .flush_prefetch_queue
   .flush_prefetch_queue:

   o32 jmp dword CS_SEL_32:start32_launcher

   .fault_end:
      hlt
      jmp .fault_end

SECTION  .start32 progbits
EXTERN __STACK_END_32
EXTERN __STACK_SIZE_32
EXTERN DS_SEL
EXTERN CS_SEL_32
EXTERN kernel32_init
EXTERN __fast_memcpy
EXTERN ___kernel_size
EXTERN ___kernel_vma_st
EXTERN ___kernel_lma_st

USE32
start32_launcher:
   ;->Inicializar la pila   
   mov ax, DS_SEL
   mov ss, ax
   mov esp, __STACK_END_32
   ;->Inicializar la pila   
   xor ebx, ebx
   mov ecx, __STACK_SIZE_32
   .stack_init:
      push ebx
      loop .stack_init
   mov esp, __STACK_END_32

   ;->Inicializar la selectores datos
   mov ds, ax
   mov es, ax
   mov gs, ax
   mov fs, ax

   ;->Desempaquetar la ROM
   ;-->kernel
   push ebp
   mov ebp, esp
   push ___kernel_size
   push ___kernel_vma_st
   push ___kernel_lma_st
   call __fast_memcpy
   leave
   cmp eax, 1           ;Analizo el valor de retorno (1 Exito -1 Fallo)
   jne .guard

   jmp CS_SEL_32:kernel32_init

   .guard:
      hlt
      jmp .guard
