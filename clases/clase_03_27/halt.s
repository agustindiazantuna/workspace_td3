USE16

etiqueta:
   db 0x0		; define byte: define un byte que contiene 0x0 y podemos referirnos a el con "etiqueta"
resb 65519 		; reserve bytes: reserva x cantidad de bytes

xor ax,ax
xor eax, eax

cli				; clear all interrupts: borra los flags de interrupciones a pesar de que al arrancar el micro ya estan asi
cld				; clear direction flag: coloca el DF en cero, de esa manera, los contadores se incrementan (con 1 se decrementa)
aca:
   hlt			; halt: detiene el micro
jmp aca			; salto en etiqueta aca: vamos de nuevo al halt por si se da la casualidad de que saltee a la proxima instruccion (nop) hasta que desborda y vuelve a la posicion 0x0..0 que ejecutara lo que encuentre
align 16		; le dice al ensamblador que lo proximo que haga tiene que tener una alineacion de 16 bytes, en este caso cae a 65531, completando los 64kbytes que debe tener nuestro codigo

