/*************************************************************************************
 * Archivo:    main.c
 * Enunciado: 
 *    Se desea implementar un programa que mediante consola reciba como argumento el
 *    numero de procesos, hilos por proceso, tiempo de vida de proceso e hilo.
 *    Cada proceso e hilo debera imprimir el valor de un contador UNICO que se debera
 *    incrementar en cada ejecucion de un proceso/hilo.
 *    Se debera gestionar y liberar adecuadamente los recursos.
 *       -Evitar zombies
 *       -No finalizar procesos/hilos antes de tiempo
 *       -Liberar los recursos utilizados
 *       -Evitar condiciones de carrera
 *       -Garantizar la adecuada concurrencia del recurso compartido
 * 
 * Autor:      Agustin Diaz Antuna
 * Fecha:      2018-09-04
 *************************************************************************************
 */


/*************************************************************************************
 * include
 *************************************************************************************
 */
#include <stdio.h>         // printf
#include <stdlib.h>        // exit
#include <sys/types.h>     
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>




/*************************************************************************************
 * struct
 *************************************************************************************
 */





/*************************************************************************************
 * handler de SIGCHLD
 *************************************************************************************
 */
void sigchld_handler(int signal)
{
   do
   {

   } while ();
}




/*************************************************************************************
 * main
 *************************************************************************************
 */
int main(int argc, char *argv[])
{
   // variables locales
   int pid, i = 0;
   int cantidad_procesos = 0, cantidad_hilos = 0;
   int tiempo_procesos = 0, tiempo_hilos = 0;
   int pid_procesos[50] = {0};

   // estructuras para sigaction
   struct sigaction nueva_accion; 
   struct sigaction vieja_accion; 




   // verificacion de los parametros recibidos
   if(argc != 5)
   {
      printf("Cantidad incorrecta de argumentos.\n");
      exit(1);
   }

   // obtengo parametros ingresados
   cantidad_procesos = atoi(argv[1]);
   cantidad_hilos    = atoi(argv[2]);
   tiempo_procesos   = atoi(argv[3]);
   tiempo_hilos      = atoi(argv[4]);




   // sobreescribo el manejador de SIGCHLD
   nueva_accion.sa_handler = sigchld_handler;
   sigemptyset(nueva_accion.sa_mask);
   // con este flag, al llegar una signal, no se borra el handler
   nueva_accion.sa_flags = SA_RESTART;

   if(sigaction(SIGCHLD, &nueva_accion, &vieja_accion) == -1)
   {
         perror("sigaction:");
         exit(1);
   }




   // crea la cantidad de procesos hijos pedida por argumento
   for(i = 0; i < cantidad_procesos; i++)
   {
      // creo cada proceso hijo
      pid_procesos[i] = fork();


      // En el caso de que no se pueda crear el proceso, fork retorna -1
      if(pid_procesos[i] == -1)
      {
         perror("fork:");
         exit(1);
      }
      else if (!pid_procesos[i])
      {
         // codigo del hijo
         child_function();
      }
   }


   // codigo del padre
   father_function();


  return 0;
}




/*************************************************************************************
 * child_function
 *************************************************************************************
 */
void child_function(void)
{
   printf ("Hijo. PID: %d\n", getpid());
   sleep (20);

   exit (0);
}




/*************************************************************************************
 * father_function
 *************************************************************************************
 */
void father_function(void)
{
   printf ("Padre. PID: %d. PID hijo: %d\n",getpid(), pid);
   sleep (10);
   wait (NULL);
   sleep (30);
}



