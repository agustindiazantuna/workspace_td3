/************************************************************************
 * Ejemplo de manejo de ipcs
 * Cola de mensajes -> receptor 2
 ************************************************************************
 */


/************************************************************************
 * include
 ************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>


/************************************************************************
 * Programa principal
 ************************************************************************
 */
struct my_msgbuf
{
   // es necesario que la estructura contenga este long
   long my_type;
   // los otros campos de la estructura son customizables
   char my_text[10];
};



/************************************************************************
 * Programa principal
 ************************************************************************
 */
int main(void)
{
   struct my_msgbuf buffer;
   // identificador de la queue, como un fid
   // no confundir, la queue no es un archivo
   int msquid;
   // variable para guardar la key
   key_t key;


   // ftok recibe:
   // - nombre de un nodo del file system
   // - un ascii cualquiera que utilizara en su algoritmo de generacion
   //   de la key
   // si falla devuelve -1
   if((key = ftok("01_ipcs_tx.c", 'B')) == -1)
   {
      perror("ftok");
      exit(1);
   }


   // msget recibe:
   // - la key a la que queremos acceder
   // - los permisos
   if((msquid = msgget(key, 0664)) == -1)
   {
      perror("msquid");
   }


   // ctrl+D = fin de archivo
   printf("Listo para recibir mensajes en el receptor 2:\n");


   // mientras gets devuelva algo que sea distinto de eof, seguimos en
   // el while
   // feof devuelve 1 cuando encuentra un eof
   while(1)
   {
      // msgsnd (message send) recibe:
      // - id de la cola de mensajes
      // - puntero a una estructura del tipo msgbuf
      // - tamanio de la estructura
      // - type: para filtrar el destinatario del mensaje, solo puede
      //   retirarlo de la cola quien tenga ese numero
      // - flag: por ahora 0, lo veremos en la lectura
      if(msgrcv(msquid, (struct  msgbuf *) &buffer, sizeof(buffer), 2, 0) == -1)
      {
         perror("msgrcv");
         exit(1);
      }
      printf("Recibi: \"%s\"\n", buffer.my_text);
   }


   return 0;
}


