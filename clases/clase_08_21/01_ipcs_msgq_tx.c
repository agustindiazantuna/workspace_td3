/************************************************************************
 * Ejemplo de manejo de ipcs
 * Cola de mensajes -> transmisor
 ************************************************************************
 */


/************************************************************************
 * include
 ************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>


/************************************************************************
 * Programa principal
 ************************************************************************
 */
struct my_msgbuf
{
   // es necesario que la estructura contenga este long
   long my_type;
   // los otros campos de la estructura son customizables
   char my_text[10];
};



/************************************************************************
 * Programa principal
 ************************************************************************
 */
int main(void)
{
   struct my_msgbuf buffer;
   // identificador de la queue, como un fid
   // no confundir, la queue no es un archivo
   int msquid;
   // variable para guardar la key
   key_t key;
   int toggle = 0;


   // ftok recibe:
   // - nombre de un nodo del file system
   // - un ascii cualquiera que utilizara en su algoritmo de generacion
   //   de la key
   // si falla devuelve -1
   if((key = ftok("01_ipcs_tx.c", 'B')) == -1)
   {
      perror("ftok");
      exit(1);
   }


   // msget recibe:
   // - la key a la que queremos acceder
   // - los permisos (en este caso con una mascara para crear la cola
   //   de mensajes si no existe)
   if((msquid = msgget(key, 0664 | IPC_CREAT)) == -1)
   {
      perror("msquid");
   }


   // ctrl+D = fin de archivo
   printf("Transmisor 1\nEscriba los mensajes a transmitir.\nPresione ^D para salir.\n\n");


   buffer.my_type = 1;


   // mientras gets devuelva algo que sea distinto de eof, seguimos en
   // el while
   // feof devuelve 1 cuando encuentra un eof
   while(gets(buffer.my_text), !feof(stdin))
   {
      // msgsnd (message send) recibe:
      // - id de la cola de mensajes
      // - puntero a una estructura del tipo msgbuf
      // - tamanio de la estructura
      // - flag: por ahora 0, lo veremos en la lectura

      if(!toggle)
      {
         buffer.my_type = 1;

         if(msgsnd(msquid, (struct  msgbuf *) &buffer, sizeof(buffer), 0) == -1)
         {
            perror("msgsnd");
         }

         toggle = !toggle;
      }
      else
      {
         buffer.my_type = 2;

         if(msgsnd(msquid, (struct  msgbuf *) &buffer, sizeof(buffer), 0) == -1)
         {
            perror("msgsnd");
         }

         toggle = !toggle;
      }
   }


   // msgctl (message control) recibe:
   // - id de la cola de mensajes
   // - comando para borrar
   // - estructura con el formato que toma la cola en memoria, con esta
   //   funcion podemos modificar parametros de la misma (como tamanio,
   //   cantidad de posiciones del buffer, etc)
   if(msgctl(msquid, IPC_RMID, NULL) == -1)
   {
      perror("msgctl");
   }


   return 0;
}


