
/************************************************************************
 * Ejemplo de fork
 * Crea un proceso hijo a partir del padre
 ************************************************************************
 */


/************************************************************************
 * include
 ************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>




/************************************************************************
 * Manejador de signals
 * Pisa la accion por defecto de SIGCHLD
 ************************************************************************
 */
void my_sigchld(int senial)
{
   wait(NULL);

   /*
      Si tenemos muchos hijos, debemos tener un array con el pid de los
      hijos y en este manejador, borrar los zombies de la lista. Para no
      sobrecargar el proceso padre, lo mas correcto seria crear un thread
      para que realice el loop dentro del vector
      Este vector debe ser una variable global ya que esta funcion no
      recibe parametros
   */
}




/************************************************************************
 * Programa principal
 ************************************************************************
 */
int main()
{
   pid_t pid;

   // Agrego un signal handler con la systemcall signal()
   if(signal(SIGCHLD, my_sigchld) == SIG_ERR)
   {
      perror("signal:");
      exit(1);
   }

   // Creo el proceso hijo
   pid = fork();

   // En el caso de que no se pueda crear el proceso, fork retorna -1
   if(pid == -1)
   {
      perror("fork:");
      exit(1);
   }
   // En el hijo esta variable vale 0, en el padre, el PID del hijo
   else if(pid == 0)
   {
      printf("Soy el hijo.\nMi PID es: %d\n\n", getpid());
   }
   else
   {
      printf("Soy el padre de %d.\nMi PID es: %d\n\n", pid, getpid());
      /* sleep coloca al proceso en estado TASK_INTERRUPTIBLE
         Si el hijo quiere finalizarse, pero el padre no puede hacerlo
         el primero queda en estado "zombie" hasta que el padre pueda
         ser finalizado
      */

      sleep(60);
   }

   exit(0);
}






