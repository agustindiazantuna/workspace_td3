
/************************************************************************
 * Ejemplo de pipe
 * Crea un proceso hijo a partir del padre
 * La salida del padre va a parar a la entrada del hijo
 * Este programa funciona igual que:
 *    ps -elf | grep " firefox "
 ************************************************************************
 */


/************************************************************************
 * include
 ************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>




/************************************************************************
 * Programa principal
 ************************************************************************
 */
int main()
{
   // pfds[lectura pipe, escritura pipe]
   int pfds[2];


   // Creo el proceso hijo
   if(!fork())
   {
      // Reemplaza la salida stdout del mismo
      dup2()

      // Reemplaza el codigo del proceso por correspondiente al argumento pasado
      execlp()
   }


   exit(0);
}






