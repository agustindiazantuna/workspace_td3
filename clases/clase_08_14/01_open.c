
/************************************************************************
 * Ejemplo de open
 * Abrir un pipe para escritura
 * 
 ************************************************************************
 */


/************************************************************************
 * include
 ************************************************************************
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


/************************************************************************
 * define
 ************************************************************************
 */
#define MENSAJE "ME QUIERO IR\n"


/************************************************************************
 * Programa principal
 ************************************************************************
 */
int main()
{
   int fd, cantidad, i;


   fd = open("my_pipe", O_WRONLY);
   if(fd == -1)
   {
      // funcion perror escribe por defecto en stderror
      perror("my_pipe open");

      // el padre puede recibir el codigo de retorno
      exit(1);
   }


   for(i = 0; i < 1000; i++)
   {
      cantidad = write(fd, MENSAJE, strlen(MENSAJE)*sizeof(char));
      if(cantidad == 1)
      {
         // funcion perror escribe por defecto en stderror
         perror("my_pipe write");

         // el padre puede recibir el codigo de retorno
         exit(1);
      }
      sleep(1);
   }


   cantidad = close(fd);
   if(cantidad == -1)
   {
      // funcion perror escribe por defecto en stderror
      perror("my_pipe close");

      // el padre puede recibir el codigo de retorno
      exit(1);
   }


   // imprime 3, ya que 0-1-2 son stdin, stdout, stderr
   printf("%d\n", fd);


   return 0;
}






