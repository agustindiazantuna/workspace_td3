# Workspace TD3

Este workspace fue utilizado durante el cursado de la materia Técnicas Digitales 3 (TD3) en la Universidad Tecnológica Nacional (UTN) Facultad Regional Buenos Aires (FRBA) año 2018.

Contiene la resolución de los trabajos prácticos de dicho año junto a una serie de ejemplos accesorios.


### Lista de TP ###

* 01_cuat: Proyecto para arquitectura x86
* 02_cuat: Proyecto sobre BBB


***

#### 01_cuat ####

Este TP consiste en el desarrollo de un sencillo OS basado en arquitectura x86.

El software permite levantar un entorno básico de trabajo en modo protegido con cambio de tareas de forma manual utilizando 2 niveles de protección.

Se desarrolló dentro del entorno de simulación Bochs.


***

#### 02_cuat ####

Esta TP propone desarrollar un sistema cliente-servidor para monitorear un sensor de temperatura.

Se utilizan los siguientes elementos: threads, ipcs, modificacion del device tree, device driver de instalación dinámica.

Se desarrolló sobre BBB.


***

### Contacto ###

Si considerás que se pueden agregar más ejercicios/ejemplos, o encontrás errores en los mismos avisame.

* agustin.diazantuna@gmail.com
* Asunto: [Workspace TD3]

***


