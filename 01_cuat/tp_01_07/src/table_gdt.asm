; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL gdt_to_gdtr
GLOBAL CS_SEL_0
GLOBAL DS_SEL_0


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
; progbits : datos definidos
; nobits : datos no inicializados
SECTION .gdt_table progbits alloc noexec nowrite


; ------------------------------------------------------------------------------
; --- struc
; ------------------------------------------------------------------------------
struc   gdtd_t                    ; Definicion de la estructura denominada gdtd_t
        .lim_00_15:     resw 1    ; Limite del segmento 00-15
        .base00_15:     resw 1    ; Direccion base del segmento bits 00-15
        .base16_23:     resb 1    ; Direccion base del segmento bits 16-23
        .prop:          resb 1    ; Propiedades
        .lim_prop:      resb 1    ; Limite del segmento 16-19 y propiedades
        .base24_31:     resb 1    ; Direccion base del segmento bits 24-31
endstruc


; ------------------------------------------------------------------------------
; --- GDT
; ------------------------------------------------------------------------------
; SEL_0 : Maximo nivel de privilegio -> kernel
; SEL_3 : Minimo nivel de privilegio -> user


GDT:
  NULL_SEL    equ $-GDT
     dq 0x0
  CS_SEL_0    equ $-GDT                     ; Define 8
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xffff
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 10011001b   ; 0x99      -------> ver si va 0x92
        at gdtd_t.lim_prop,  db 11001111b   ; 0xCF
        at gdtd_t.base24_31, db 0
     iend
  DS_SEL_0    equ $-GDT                     ; Define 16
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xffff
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 10010010b   ; 0x92      -------> ver si va 0x91
        at gdtd_t.lim_prop,  db 11001111b
        at gdtd_t.base24_31, db 0
     iend
  GDT_LENGTH  equ $-GDT                     ; Define 24


gdt_to_gdtr:
  dw GDT_LENGTH-1            ; Tamaño de la tabla en reg 16 bits
  dd GDT                     ; Dirección relativa en reg 32 bits







