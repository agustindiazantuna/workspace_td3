; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL isr0_handler
GLOBAL isr0_handler_DE
GLOBAL isr2_handler_NMI
GLOBAL isr3_handler_BP
GLOBAL isr4_handler_OF
GLOBAL isr5_handler_BR
GLOBAL isr6_handler_UD
GLOBAL isr7_handler_NM
GLOBAL isr8_handler_DF
GLOBAL isr10_handler_TS
GLOBAL isr11_handler_NP
GLOBAL isr12_handler_SS
GLOBAL isr13_handler_GP
GLOBAL isr14_handler_PF
GLOBAL isr16_handler_MF
GLOBAL isr17_handler_AC
GLOBAL isr18_handler_MC
GLOBAL isr19_handler_XF
GLOBAL isr20_31_handler
GLOBAL isr32_47_handler


; ------------------------------------------------------------------------------
; progbits : datos definidos
; nobits : datos no inicializados
SECTION .isr progbits


; ------------------------------------------------------------------------------
; --- default_isr
; ------------------------------------------------------------------------------

default_isr:
   nop
   BOCHS_STOP
   nop
   hlt


isr0_handler_DE:
   mov edx, 0
   jmp default_isr


isr2_handler_NMI:
   mov edx, 2
   jmp default_isr


isr3_handler_BP:
   mov edx, 3
   jmp default_isr


isr4_handler_OF:
   mov edx, 4
   jmp default_isr


isr5_handler_BR:
   mov edx, 5
   jmp default_isr


isr6_handler_UD:
   mov edx, 6
   jmp default_isr


isr7_handler_NM:
   mov edx, 7
   jmp default_isr


isr8_handler_DF:
   mov edx, 8
   jmp default_isr


isr10_handler_TS:
   mov edx, 10
   jmp default_isr


isr11_handler_NP:
   mov edx, 11
   jmp default_isr


isr12_handler_SS:
   mov edx, 12
   jmp default_isr


isr13_handler_GP:
   mov edx, 13
   jmp default_isr


isr14_handler_PF:
   mov edx, 14
   jmp default_isr


isr16_handler_MF:
   mov edx, 16
   jmp default_isr


isr17_handler_AC:
   mov edx, 17
   jmp default_isr


isr18_handler_MC:
   mov edx, 18
   jmp default_isr


isr19_handler_XF:
   mov edx, 19
   jmp default_isr


isr20_31_handler:
   mov edx, 20
   jmp default_isr


isr32_47_handler:
   mov edx, 32
   jmp default_isr





