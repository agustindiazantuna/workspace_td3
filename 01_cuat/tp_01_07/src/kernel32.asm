; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define S_PRESSED             0x1F
%define Y_PRESSED             0x15
%define U_PRESSED             0x16
%define I_PRESSED             0x17
%define O_PRESSED             0x18
%define P_PRESSED             0x19
%define NULL_KEY              0x00
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN td3_kybrd_poll
EXTERN __TABLA_DIGITOS_START
EXTERN __TABLA_DIGITOS_SIZE


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL kernel32


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .kernel32 progbits


; ------------------------------------------------------------------------------
; --- kernel32
; ------------------------------------------------------------------------------
kernel32:

;   BOCHS_STOP

   .while1:

      call td3_kybrd_poll


      ; Si no hay tecla vuelvo
      cmp eax, NULL_KEY
      je .while1


      ; Si presionaron Y -> DE 0x00    Division Error
      cmp eax, Y_PRESSED
      jne .checkUD
         ; Division por cero
         mov eax, 0x80
         mov ecx, 0x00
         div ecx


      .checkUD:
      ; Si presionaron U -> UD 0x06    Undefined Opcode
      cmp eax, U_PRESSED
      jne .checkDF
         ; Ejecuto 0xFFFF_FFFF como opcode
         mov dword [.die], 0xFFFFFFFF
         .die:
            mov eax,0x1234


      .checkDF:
      ; Si presionaron I -> DF 0x80    Double Fault
      cmp eax, I_PRESSED
      jne .checkGP
         ; No se como generarlo
         int 0x08


      .checkGP:
      ; Si presionaron O -> GP 0x0D    General Protection
      cmp eax, O_PRESSED
      jne .checkPF 
         ; Cargo en ss un numero al azar
         mov ax, 0xFFFF
         mov ss, ax


      .checkPF:
      ; Si presionaron P -> PF 0x0E    Page Fault
      cmp eax, P_PRESSED
      jne .while1
         ; No se como generarlo
         int 0x0E


      jmp .while1




%if 0
   mov edi, __TABLA_DIGITOS_START

   .while1:

      call td3_kybrd_poll

      ; Si no hay tecla vuelvo
      cmp eax, NULL_KEY
      je .while1

      ; Si presionaron S -> halt
      cmp eax, S_PRESSED
      jne .kybrd_save
      hlt

      ; Si no, guardo teclas
      .kybrd_save:
         mov [edi], al
         inc edi

      ; Si satura el buffer circular, reinicio
;      cmp edi, (__TABLA_DIGITOS_START + __TABLA_DIGITOS_SIZE)
      cmp edi, (__TABLA_DIGITOS_START + 0x10)
      jne .while1

      mov edi, __TABLA_DIGITOS_START

      jmp .while1
%endif



   
   .guard:
      hlt
      jmp .guard







