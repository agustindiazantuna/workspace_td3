; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE16


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "processor-flags.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
;EXTERN __STACK_START_16
;EXTERN __STACK_END_16
EXTERN A20_Enable_No_Stack
EXTERN gdt_to_gdtr
EXTERN PIT_Set_Counter0
EXTERN PIC_Config
EXTERN CS_SEL_0
EXTERN kernel32_init


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL start16
GLOBAL A20_Enable_No_Stack_return


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start16 progbits


; ------------------------------------------------------------------------------
; --- start16
; ------------------------------------------------------------------------------
start16:
   ; Verificar que el uP no este en fallo
   test  eax, 0x00
   jne   ..@fault_end


   ; ---------------> early_board_init
   ; **************************************** puede que vaya
      ;   xor   eax, eax
      ;   mov   cr3, eax             ;Invalidar TLB

      ; Habilito A20
      jmp A20_Enable_No_Stack
      A20_Enable_No_Stack_return:

;      mov   ax, cs
;      mov   ds, ax
;      mov   ax, __STACK_START_16
;      mov   ss, ax
;      mov   sp, __STACK_END_16
   ; early_board_init <---------------

   ; **************************************** puede que vaya
;   ;->Deshabilitar cache<-
;   mov   eax, cr0
;   or    eax, (X86_CR0_NW | X86_CR0_CD)
;   mov   cr0, eax
;   wbinvd

   ; Cargo GDT
   o32 lgdt  [cs:gdt_to_gdtr]
   

   ; Callout para agregar funciones de inicializacion de los chipset de la placa
   ; ---------------> late_board_init
   ; **************************************** puede que vaya
;      mov cx, 0x2                            ;Interrumpir cada 2mseg
;      call PIT_Set_Counter0

;      mov bx, 0x2820                         ;Base PIC0=0x20 PIC1=0x28
;      call PIC_Config
   ; late_board_init <---------------


   ; Establecer el uP en MP
   smsw  ax
   or    ax, X86_CR0_PE
   lmsw  ax


   o32 jmp dword CS_SEL_0:start32




   ; Error
   ..@fault_end:
      hlt
      jmp ..@fault_end








; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx
%define PIC1                  0x21
%define PIC2                  0xA1


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN DS_SEL_0
EXTERN __STACK_END
EXTERN __STACK_SIZE
EXTERN td3_memcopy
EXTERN ___kernel_size
EXTERN ___kernel_vma_st
EXTERN ___kernel_lma_st
EXTERN ___irqsize
EXTERN ___irq_lma
EXTERN ___irq_vma
EXTERN ___idtsize
EXTERN ___idt_lma
EXTERN ___idt_vma
EXTERN idt_to_idtr
EXTERN CS_SEL_0
EXTERN kernel32


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start32 progbits


; ------------------------------------------------------------------------------
; --- start32
; ------------------------------------------------------------------------------
start32:

   ; Inicializar la selectores datos
   mov ax, DS_SEL_0   
   mov ds, ax
   mov es, ax
   mov gs, ax
   mov fs, ax

   ; Inicializar la pila en segmento DS  
   mov ss, ax
   mov esp, __STACK_END

   ; Limpiar el stack
   xor ebx, ebx
   mov ecx, __STACK_SIZE

   .stack_init:
      push ebx
      loop .stack_init
   mov esp, __STACK_END


   ; Desempaquetar la ROM
   ; kernel
   push ebp
   mov ebp, esp
   push ___kernel_size        ;  arg3  -> cantidad
   push ___kernel_lma_st      ;  arg2  -> origen
   push ___kernel_vma_st      ;  arg1  -> destino
   call td3_memcopy
   leave

   ; isr
   push ebp
   mov ebp, esp
   push ___irqsize            ;  arg3  -> cantidad
   push ___irq_lma            ;  arg2  -> origen
   push ___irq_vma            ;  arg1  -> destino
   call td3_memcopy
   leave

   ; idt
   push ebp
   mov ebp, esp
   push ___idtsize            ;  arg3  -> cantidad
   push ___idt_lma            ;  arg2  -> origen
   push ___idt_vma            ;  arg1  -> destino
   call td3_memcopy
   leave

   ; Cargar IDT
   lidt [idt_to_idtr]

   ; Configurar PIC
   ; Source:     https://en.wikibooks.org/wiki/X86_Assembly/Programmable_Interrupt_Controller
   ; Base PIC0=0x20 PIC1=0x28
   mov bx, 0x2820
   call PIC_Config

   ; Leyendo DATAPORT obtenemos IMR register
   ; Escribiendo DATAPORT seteamos IMR register
   ; Seteando 1 en el bit_N se deshabilita la IRQ_N
   or ax, 0xFF

   ; Deshabilitar las IRQ de ambos PIC
   out PIC1, ax
   out PIC2, ax

   ; Habilitar interrupciones
   sti




   jmp CS_SEL_0:kernel32







