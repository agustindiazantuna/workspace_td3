; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define PS2_SREG              0x64
%define PS2_DPORT             0x60
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL td3_kybrd_poll


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .drivers progbits


; ------------------------------------------------------------------------------
; --- keyboard
; ------------------------------------------------------------------------------

;  Funcion pedida:
;  byte td3_kybrd_poll(void);
;  Si ret = 0x00 -> No hay tecla

td3_kybrd_poll:

;  STATUS REGISTER   ->    PORT 0x64   
;  0  Output buffer status (0 = empty, 1 = full)
;     (must be set before attempting to read data from IO port 0x60)
;  1  Input buffer status (0 = empty, 1 = full)
;     (must be clear before attempting to write data to IO port 0x60 or IO port 0x64)
;  2  System Flag
;     Meant to be cleared on reset and set by firmware (via. PS/2 Controller Configuration Byte) if the system passes self tests (POST)
;  3  Command/data (0 = data written to input buffer is data for PS/2 device, 1 = data written to input buffer is data for PS/2 controller command)
;  4  Unknown (chipset specific)
;     May be "keyboard lock" (more likely unused on modern systems)
;  5  Unknown (chipset specific)
;     May be "receive time-out" or "second PS/2 port output buffer full"
;  6  Time-out error (0 = no error, 1 = time-out error)
;  7  Parity error (0 = no error, 1 = parity error)
;  
;  Source:     https://wiki.osdev.org/PS/2_Keyboard


   ; Me fijo si hay algo en el buffer
   xor eax, eax               ; Limpio el registro eax
   in al, PS2_SREG            ; STATUS_REGISTER (0x64) lo guardo en eax_low
   bt eax, 0x00               ; Guardo eax[0] en eflags[0] = bit CF flag
                              ; Si eax[0] = 1 => STATUS_REGISTER[0] = 1 => output buffer is full
   jnc .kybrd_ret_null        ; Si CF = 0 => el buffer esta vacio

   ; Si hay algo en el buffer me fijo si el codigo es de pressed o release
   in al, PS2_DPORT           ; DATA_PORT (0x60) lo guardo en eax_low
   bt eax, 0x07               ; Guarda el bit 7 de eax en CF flag
   jc .kybrd_ret_null         ; Si CF = 1 => codigo release, no lo considero

   ret                        ; Retorno la tecla presionada
   
   .kybrd_ret_null:
      mov eax, 0x00
      ret                     ; Retorno 0x00 si no hay tecla


   .guard:
      hlt
      jmp .guard







