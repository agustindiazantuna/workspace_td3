; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define NULL_KEY              0xEE
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN td3_kybrd_poll
EXTERN __TABLA_DIGITOS_START
EXTERN __TABLA_DIGITOS_SIZE
EXTERN kybrd_data
EXTERN alfabeto_h
EXTERN read_table_h


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL kernel32


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .kernel32 progbits


; ------------------------------------------------------------------------------
; --- kernel32
; ------------------------------------------------------------------------------
kernel32:

   mov byte [kybrd_data], NULL_KEY

   while1:
      ; Tecla leida
      mov byte eax, [kybrd_data]

      ; Si no hay tecla vuelve al while1
      cmp byte eax, NULL_KEY
      je while1

      ; Si es un codigo release vuelve al while1
      bt eax, 0x07           ; Guarda el bit 7 de eax en CF flag
      jc while1              ; Si CF = 1 => codigo release

      ; Funcion filtrado tecla + guardado
      push ebp
      mov ebp, esp
      push eax
      call alfabeto_h
      leave

      ; Limpio buffer
      mov byte [kybrd_data], NULL_KEY

      ; Compruebo escritura
      push eax
      call read_table_h

      jmp while1



   
   .guard:
      hlt
      jmp .guard







