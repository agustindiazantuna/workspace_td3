/** 
   \file    functions.c   
   \version 01.00
   \brief   Funciones utilizadas en kernel.asm

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"
#include "../inc/keyboard.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */
// Ring buffer
byte ring_buffer[16] __DATA__ = {0};
byte count __DATA__ = 0;


// Tabla digitos
qword tabla_digitos[1000] __TABLA_DIGITOS__ ;
byte td_index __DATA__ = 0;
qword suma_tabla_digitos __DATA__ = 0;


// Contador de ingresos de la tabla de digitos
extern dword tabla_digitos_count;


/* ------------------------------------------------------------------------------
 * --- alfabeto_h
 * ------------------------------------------------------------------------------
 */
__FUNCTIONS__ void alfabeto_h(byte key)
{
   byte number = 0, i = 0;
   qword table_entry = 0;


   // Filtro letra
   if( key < KEY_0_PRESSED )
      number = key - 1;
   else if( key == KEY_0_PRESSED )
      number = 0;
   else if( key == KEY_A_PRESSED )
      number = 0xA;
   else if( key == KEY_B_PRESSED )
      number = 0xB;
   else if( key == KEY_C_PRESSED )
      number = 0xC;
   else if( key == KEY_D_PRESSED )
      number = 0xD;
   else if( key == KEY_E_PRESSED )
      number = 0xE;
   else if( key == KEY_F_PRESSED )
      number = 0xF;
   else if( key == KEY_ENTER_PRESSED )
   {
      for(i = 0; i < count; i++)
      {
         table_entry =  ring_buffer[i] | (table_entry << 4);
      }

      tabla_digitos[td_index] = table_entry;

      tabla_digitos_count++;

      td_index++;
      count = 0;
   }
   else
      return;


   // Guardo en el ring buffer
   ring_buffer[count] = number;
   count++;


   return;
}


/* ------------------------------------------------------------------------------
 * --- read_table_h
 * ------------------------------------------------------------------------------
 */
__FUNCTIONS__ void read_table_h(byte key)
{
   // Filtro letra
   if( key == KEY_R_PRESSED )
   {
      BOCHS_STOP
         asm("mov %0,%%eax"::"r"(suma_tabla_digitos));
   }

   return;
}







