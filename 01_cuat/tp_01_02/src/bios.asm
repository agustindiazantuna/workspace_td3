; -------------------------------------------------------------------------------
; --- Directivas
; -------------------------------------------------------------------------------

USE16			; Le indicamos al codigo que trabaje en modo 16bits
				; Los registros ocupan 2 bytes en stack

				




; -------------------------------------------------------------------------------
; --- Defines
; -------------------------------------------------------------------------------

%define ROM_SIZE		(64*1024)
%define ROM_START		0x0000
%define RESET_VECTOR	0xfff0

%define DESTINO_1		0x0000
%define DESTINO_2		0x0000


extern __pepito
global start16


; -------------------------------------------------------------------------------
; --- Start16
; -------------------------------------------------------------------------------

start16:	

	; Defino registros del STACK --> ss:sp
	mov eax, __pepito;0xF000
	mov ss, eax

	mov eax, 0xFFFF
	mov esp, eax

	; Defino segmento extra para el indice destino
	mov eax, 0x0000
	mov es, eax					;	es:di

	; Paso parametros y llamo a la funcion
	push dword (ROM_SIZE-1)	;	Cantidad
	push dword ROM_START		;	Origen
	push dword DESTINO_1		;	Destino

	call td3_memcopy

	; Acomodo sp
	pop eax	;	ESP + 4 * nro_argumentos
				;	mov en cantidad de bytes
	pop eax
	pop eax

idle:
	nop
	jmp idle






; -------------------------------------------------------------------------------
; --- Funcion td3_memcopy
; -------------------------------------------------------------------------------

;	Funcion pedida:
;	void *td3_memcopy(void *destino, const void *origen, unsigned int num_bytes);

td3_memcopy:

	; Pusheo registros implicados
	push ebp       ;  Pusheo Base Pointer para guardar su contenido
	mov ebp, esp   ;  En el BP guardo el SP que esta al nivel de la pila

	push edi
	push esi
	push ecx

	; Obtengo los argumentos del stack de 16bits
	;	ebp_h		<---	ebp
	;	ebp_l
	;	ip
	;	arg1_h		<---	ebp+6
	;	arg1_l
	;	arg2_h		<---	ebp+10
	;	arg2_l
	;	arg3_h		<---	ebp+14
	;	arg3_l
	mov edi, [ebp+6]			;  1er arg
	mov esi, [ebp+10]			;  2do arg
	mov ecx, [ebp+14]			;  3er arg

	; Realizo funcion
	lazo:
		mov ax,[cs:si]
		mov [es:di],ax
		add di,2
		add si,2
	loop lazo

	; Popeo registros implicados devuelvo lo pedido
	pop dword ecx
	pop dword esi
	pop dword edi
	pop dword ebp

	mov eax, [esp+2]          ;  Devuelvo el puntero destino

	ret                     ;  Popeo IP






; -------------------------------------------------------------------------------
; --- El codigo arranca en 0xFFFF0
; -------------------------------------------------------------------------------

	CODE_LENGTH equ ($-start16)

	; ocupa espacio con nop para posicionar el codigo en 0xFFFF0
	times (RESET_VECTOR - ROM_START - CODE_LENGTH) nop	

reset_vector:
	cli	     		; deshabilito las interrupciones
	cld             ; Clear Direction Flag: DF = 0 -> Los contadores se incrementan

	; El procesador arranca en 0xFFFF0 y pasamos a 0x00000 (comienzo de la ROM)
	jmp start16

	; Alineo para que la ROM ocupe 64k
	align 16		; Alinea a 16 bytes, en este caso cae a 65531





