; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define PIC1_CMD              0x20
%define PIC1_DATA             0x21
%define PIC2_CMD              0xA0
%define PIC2_DATA             0xA1


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN PIC_Config
EXTERN PIT_Set_Counter0
EXTERN init_irq_ret


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL init_irq


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start32 progbits


; ------------------------------------------------------------------------------
; --- init_irq
; ------------------------------------------------------------------------------
init_irq:

   ; Configurar PIC
   ; Source:
   ;     https://en.wikibooks.org/wiki/X86_Assembly/Programmable_Interrupt_Controller
   ;     https://wiki.osdev.org/8259_PIC
   ; Base PIC1 = 0x20 (32) - PIC2 = 0x28 (40)

   .pic_config:
      mov bx, 0x2820
      call PIC_Config


   ; in DATAPORT para leer IMR register
   ; out DATAPORT para setear IMR register
   ; Seteando 1 en el bit_N se deshabilita la IRQ_N

   ; Deshabilitar las IRQ de ambos PIC
   .disable_irq:
      in al, PIC1_DATA
      or al, 0xFF
      out PIC1_DATA, al

      in al, PIC2_DATA
      or al, 0xFF   
      out PIC2_DATA, al


   ; Configurar el PIT para que interrupa cada 50ms
   ;
   ; Source:
   ;     https://1984.lsi.us.es/wiki-ssoo/index.php/Reloj_hardware:_Intel_8253
   ;     https://en.wikipedia.org/wiki/Intel_8253
   ;     https://wiki.osdev.org/Programmable_Interval_Timer
   .pit_config:
      mov cx, 50
      call PIT_Set_Counter0


   ; PIC1
   ; 0   IRQ_0 -> 32    Timer tick
   ; 1   IRQ_1 -> 33    Keyboard
   ; 2   IRQ_2 -> 34    INT PIC2
   ; 3   IRQ_3          .
   ; 4   IRQ_4          .
   ; 5   IRQ_5          .
   ; 6   IRQ_6          .
   ; 7   IRQ_7          .

   ; Habilito IRQ_0 e IRQ_1
   .enable_irq:
      in al, PIC1_DATA
      and al, 0xFC      ; 1111_1100b
      out PIC1_DATA, al


   ; Habilitar interrupciones
   sti


   jmp init_irq_ret







