; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "processor-flags.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define NULL_KEY              0xEE
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN __PAG_TABLE_START
EXTERN kybrd_data
EXTERN alfabeto_h
EXTERN read_table_h
EXTERN timer_count
EXTERN last_count
EXTERN tabla_digitos_count
EXTERN add_tabla_digitos


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL kernel32


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .kernel progbits


; ------------------------------------------------------------------------------
; --- kernel32
; ------------------------------------------------------------------------------
kernel32:
BOCHS_STOP
   .init_paging:
      ; cargo cr3
      mov eax, __PAG_TABLE_START
;      or eax, X86_CR3_PCD
;      or eax, X86_CR3_PWT
      mov cr3, eax

      ; habilito paginacion
      mov eax, cr0
;      or eax, X86_CR0_PG
      mov cr0, eax


   mov byte [kybrd_data], NULL_KEY

   
   while1:

      up_halt:
         hlt

         ; Si pasaron 500ms
         cmp byte [timer_count], 0x0A
         je suma_tabla_digitos

         ; Si la interrupcion fue de teclado
         mov al, [last_count]
         cmp byte [timer_count], al
         je keyboard_routine

         ; Sino fue de timer pero no pasaron 500ms
         mov al, [timer_count]
         mov byte [last_count], al

         jmp up_halt 


      suma_tabla_digitos:
         call add_tabla_digitos

         ; Limpio cuenta
         and byte [timer_count], 0x00

         jmp up_halt


      keyboard_routine:
         ; Tecla leida
         mov eax, [kybrd_data]

         ; Si es un codigo release vuelve al up_halt
         bt eax, 0x07           ; Guarda el bit 7 de eax en CF flag
         jc up_halt             ; Si CF = 1 => codigo release

         ; Funcion filtrado tecla + guardado
         push eax
         call alfabeto_h

         ; Limpio buffer
         mov byte [kybrd_data], NULL_KEY

         ; Compruebo escritura
         push eax
         call read_table_h

         jmp while1




   .guard:
      hlt
      jmp .guard







