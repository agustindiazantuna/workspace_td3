; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE16

; ------------------------------------------------------------------------------
; --- Externs
; ------------------------------------------------------------------------------
EXTERN start16

; ------------------------------------------------------------------------------
; --- Sections
; ------------------------------------------------------------------------------
SECTION .reset_vector

reset_vector:
   ; Limpio flags
   cli            ; deshabilito las interrupciones
   cld            ; Clear Direction Flag: DF = 0 -> Los contadores se incrementan

   ; El procesador arranca en 0xFFFF_FFF0 y pasamos a 0xFFF0_0000 (comienzo de la ROM)
   jmp start16







