; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE16

; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "processor-flags.h"

; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN __STACK_START_16
EXTERN __STACK_END_16
EXTERN A20_Enable_No_Stack
EXTERN rom_gdtr
EXTERN PIT_Set_Counter0
EXTERN PIC_Config
EXTERN CS_SEL_0
EXTERN kernel32_init

; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL start16
GLOBAL A20_Enable_No_Stack_return

; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start16 progbits

; ------------------------------------------------------------------------------
; --- start16
; ------------------------------------------------------------------------------
start16:
   test  eax, 0x0                    ; Verificar que el uP no este en fallo
   jne   ..@fault_end

   mov   ax, __STACK_START_16
   mov   ss, ax
   mov   sp, __STACK_END_16 
 

   ; ---------------> early_board_init
      ;   xor   eax, eax
      ;   mov   cr3, eax             ;Invalidar TLB

      ; Habilito A20
      jmp A20_Enable_No_Stack
      A20_Enable_No_Stack_return:

      mov   ax, cs
      mov   ds, ax
      mov   ax, __STACK_START_16
      mov   ss, ax
      mov   sp, __STACK_END_16
   ; early_board_init <---------------


;   ;->Deshabilitar cache<-
;   mov   eax, cr0
;   or    eax, (X86_CR0_NW | X86_CR0_CD)
;   mov   cr0, eax
;   wbinvd

   o32 lgdt  [cs:rom_gdtr]          ; Cargo GDT
   

   ; Callout para agregar funciones de inicializacion de los chipset de la placa
   ; ---------------> late_board_init
      mov cx, 0x2                            ;Interrumpir cada 2mseg
      call PIT_Set_Counter0

      mov bx, 0x2820                         ;Base PIC0=0x20 PIC1=0x28
      call PIC_Config
   ; late_board_init <---------------


   ; Establecer el up en MP
   smsw  ax
   or    ax, X86_CR0_PE
   lmsw  ax

   jmp .flush_prefetch_queue  ; Para vaciar el pipeline de instrucciones que tiene instrucciones de 32bits
   .flush_prefetch_queue:

   o32 jmp dword CS_SEL_0:start32




   ; Error
   ..@fault_end:
      hlt
      jmp ..@fault_end








; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32

; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN DS_SEL_0
EXTERN __STACK_END_32
EXTERN __STACK_SIZE_32
EXTERN ___kernel_size
EXTERN ___kernel_vma_st
EXTERN ___kernel_lma_st
EXTERN __my_memcpy
EXTERN CS_SEL_0
EXTERN kernel32

; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start32 progbits

; ------------------------------------------------------------------------------
; --- start32
; ------------------------------------------------------------------------------
start32:
   ; Inicializar la pila en segmento DS  
   mov ax, DS_SEL_0
   mov ss, ax

   mov esp, __STACK_END_32

   xor ebx, ebx
   mov ecx, __STACK_SIZE_32

   .stack_init:               ; Limpio el stack
      push ebx
      loop .stack_init
   mov esp, __STACK_END_32

   ; Inicializar la selectores datos
   mov ds, ax
   mov es, ax
   mov gs, ax
   mov fs, ax

   ; Desempaquetar la ROM
   ; kernel
   push ebp
   mov ebp, esp
   push ___kernel_size           ;  arg3  -> cantidad
   push ___kernel_lma_st         ;  arg2  -> origen
   push ___kernel_vma_st         ;  arg1  -> destino
   call __my_memcpy
   leave




   jmp CS_SEL_0:kernel32







