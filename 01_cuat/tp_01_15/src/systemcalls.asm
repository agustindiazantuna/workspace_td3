;-------------------------------------------------------------------------------
;|  Título:         System calls                                               |
;|  Versión:        1.0                     Fecha:  16/06/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Contiene:                                                              |
;|          - APIs de acceso publico para acceder a las System Calls           |
;|          - Handler de la IRQ 0x80                                           |
;|          - Rutinas de las System Calls ejecutadas por el kernel             |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 16/06/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "config.h"
%include "processor-flags.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx

%define DEFAULT_EFLAGS        0x202
%define SYSCALL_hlt           0x01
%define SYSCALL_read          0x02
   
%define table_attr_default    0x0007
%define page_attr_u_rw        0x0007


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN tabla_digitos
EXTERN td3_memcopy_RAM
EXTERN check_paging


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL isr_handler_SYSTEMCALLS

; user
GLOBAL td3_halt
GLOBAL td3_read


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .systemcall_nivel3 progbits


; ------------------------------------------------------------------------------
; --- td3_halt
; ------------------------------------------------------------------------------
td3_halt:

   mov eax, SYSCALL_hlt
   int 0x80

   ret


; ------------------------------------------------------------------------------
; --- td3_read
; ------------------------------------------------------------------------------
;(void *buffer, unsigned int num_bytes);
td3_read:

   push ebx
   push ecx

   mov eax, SYSCALL_read
   mov ebx, [esp + 0x0C]   ; *buffer
   mov ecx, [esp + 0x10]   ; num_bytes

   int 0x80

   pop ecx
   pop ebx

   ret












; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .isr200_systemcall progbits


; ------------------------------------------------------------------------------
; --- isr_handler_SYSTEMCALLS
; ------------------------------------------------------------------------------
isr_handler_SYSTEMCALLS:
   
   ; ---------------------------------------------------------
   ; --- Stack frame de Interrup:
   ; ---------------------------------------------------------
   ; eip       <- esp actual (nivel 0)
   ; cs
   ; eflags
   ; esp
   ; ss
   ; ---------------------------------------------------------

   identify_syscall:
      cmp eax, SYSCALL_hlt
      je is_hlt

      cmp eax, SYSCALL_read
      je is_read

      iret

   is_hlt:
      jmp systemcall_halt

   is_read:
      jmp systemcall_read

   .guard:
      hlt
      jmp .guard












; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .systemcall_kernel progbits


EXTERN DS_SEL_0
EXTERN DS_SEL_3
; ------------------------------------------------------------------------------
; --- systemcall_halt
; ------------------------------------------------------------------------------
systemcall_halt:

   ; ---------------------------------------------------------------------------
   ; --- Stack frame
   ; ---------------------------------------------------------------------------
   ; esp
   ; ss        <- hacia arriba debo agregar para cumplir con el scheduler
   ; eip       <- hacia abajo es de la tarea 0
   ; cs
   ; eflags
   ; esp
   ; ss
   ; ---------------------------------------------------------------------------

   .enable_interruptions:
      sti

   .halt:
      hlt

   iret


; ------------------------------------------------------------------------------
; --- systemcall_read
; ------------------------------------------------------------------------------
systemcall_read:

   ; ---------------------------------------------------------------------------
   ; --- Argumentos
   ; ---------------------------------------------------------------------------
   ; ebx    : *buffer
   ; ecx    : num_bytes
   ; ---------------------------------------------------------------------------

   .check_if_destiny_begin_is_in_page:
      pushad

      push ebp
      mov ebp, esp
      push page_attr_u_rw
      push table_attr_default
      push ebx
      mov eax, cr3
      push eax 
      call check_paging
      leave

      cmp eax, 0x00
      jne exit
      popad


   .check_if_destiny_end_is_in_page:
      pushad

      push ebp
      mov ebp, esp
      push page_attr_u_rw
      push table_attr_default
      add ebx, ecx
      push ebx
      mov eax, cr3
      push eax 
      call check_paging
      leave

      cmp eax, 0x00
      jne exit
      popad


   make_copy:
      push ebp
      mov ebp, esp
      push ecx             ;  arg3  -> cantidad
      push tabla_digitos   ;  arg2  -> origen
      push ebx             ;  arg1  -> destino
      call td3_memcopy_RAM
      leave

      iret

   exit:
      popad
      iret






