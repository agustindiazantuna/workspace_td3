;-------------------------------------------------------------------------------
;|  Título:         SIMD                                                       |
;|  Versión:        1.0                     Fecha:  25/08/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Contiene:                                                              |
;|          - APIs de acceso publico para acceder a las System Calls           |
;|          - Handler de la IRQ 0x80                                           |
;|          - Rutinas de las System Calls ejecutadas por el kernel             |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 25/08/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx
%define num_qword             20


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN tabla_digitos_t1
EXTERN tabla_digitos_t2


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL t1_simd
GLOBAL t2_simd


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss_t1 nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss_t1
; ------------------------------------------------------------------------------

   acumulado_t1:
      resd 1








; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .TEXT_t1 progbits


; ------------------------------------------------------------------------------
; --- t1_simd
; ------------------------------------------------------------------------------
t1_simd:
   pushad

   ; edx contiene la cantidad de loops
   mov edx, num_qword/2
   ; ecx es el indice dentro del vector
   xor ecx, ecx
   mov [acumulado_t1], ecx

   .lazofor:
      movdqu xmm1, [tabla_digitos_t1 + (ecx * 4)]
      
      ; para realizar la suma horizontal de 4 dword en el mismo registro
      phaddd xmm1, xmm1
      phaddd xmm1, xmm1

      ; obtengo el resultado en ebx
      movd ebx, xmm1
      add [acumulado_t1], ebx

      inc ecx
      cmp ecx, edx
      jne .lazofor

   popad
   mov eax, [acumulado_t1]
   ret












; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss_t2 nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss_t2
; ------------------------------------------------------------------------------

   acumulado_t2:
      resb 1








; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .TEXT_t2 progbits



; ------------------------------------------------------------------------------
; --- t2_simd
; ------------------------------------------------------------------------------
t2_simd:
   pushad

   ; edx contiene la cantidad de loops
   mov edx, num_qword/2
   ; ecx es el indice dentro del vector
   xor ecx, ecx
   mov [acumulado_t1], ecx

   .lazofor:
      movdqu xmm1, [tabla_digitos_t1 + (ecx * 4)]
      
      ; para realizar la suma horizontal de 8 word en el mismo registro
      phaddw xmm1, xmm1
      phaddw xmm1, xmm1
      phaddw xmm1, xmm1

      ; obtengo el resultado en ebx
      movd ebx, xmm1
      and ebx, 0x0000FFFF
      add [acumulado_t2], ebx

      inc ecx
      cmp ecx, edx
      jne .lazofor

   popad
   mov eax, [acumulado_t2]
   ret







