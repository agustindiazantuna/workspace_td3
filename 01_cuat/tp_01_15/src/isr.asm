;-------------------------------------------------------------------------------
;|  Título:         Interrupt Handlers                                         |
;|  Versión:        1.0                     Fecha:  19/06/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Posee los handlers de todas las interrupciones, incluye:               |
;|          - Int default                                                      |
;|          - Int 14: #PF                                                      |
;|          - Int 32: PIT - Timer                                              |
;|          - Int 33: PIT - Teclado                                            |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 19/06/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%include "processor-flags.h"
%include "config.h"
%include "tss-table.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx

%define PS2_DPORT             0x60
%define PS2_SREG              0x64
%define PS2_ACK               0x20


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
; ISR7
EXTERN TSS_TASK_0
EXTERN last_task_simd

; ISR14
EXTERN ptree_entry
EXTERN page_addr_phy

; ISR32
EXTERN timer_count

; ISR33
EXTERN alfabeto_h
EXTERN read_table_h


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL isr0_handler_DE
GLOBAL isr2_handler_NMI
GLOBAL isr3_handler_BP
GLOBAL isr4_handler_OF
GLOBAL isr5_handler_BR
GLOBAL isr6_handler_UD
GLOBAL isr7_handler_NM
GLOBAL isr8_handler_DF
GLOBAL isr10_handler_TS
GLOBAL isr11_handler_NP
GLOBAL isr12_handler_SS
GLOBAL isr13_handler_GP
GLOBAL isr14_handler_PF
GLOBAL isr16_handler_MF
GLOBAL isr17_handler_AC
GLOBAL isr18_handler_MC
GLOBAL isr19_handler_XF
GLOBAL isr33_handler_KEYBOARD


; ------------------------------------------------------------------------------
; progbits : datos definidos
; nobits : datos no inicializados
SECTION .isr progbits


; ------------------------------------------------------------------------------
; --- default_isr
; ------------------------------------------------------------------------------
; Source:
;           https://wiki.osdev.org/Exceptions
;           https://wiki.osdev.org/Interrupts
;           

default_isr:
   nop
   BOCHS_STOP
   nop
   hlt




isr0_handler_DE:
   mov edx, 0
   jmp default_isr




isr2_handler_NMI:
   mov edx, 2
   jmp default_isr




isr3_handler_BP:
   mov edx, 3
   jmp default_isr




isr4_handler_OF:
   mov edx, 4
   jmp default_isr




isr5_handler_BR:
   mov edx, 5
   jmp default_isr




isr6_handler_UD:
   mov edx, 6
   jmp default_isr




; ------------------------------------------------------------
; ------------------------------------------------------------
isr7_handler_NM:
   pushad

   ; deshabilito esta interrupcion CR0.TS (bit 3)
   clts

   ; guardo cr3 actual
   mov ebx, cr3

   is_the_first_time:
      cmp [last_task_simd], dword 0x00
      je exit_isr7

   not_first_time:
      cmp ebx, [last_task_simd]
      je exit_isr7

   another_task:
      ; restauro cr3 de la ultima tarea en usar simd
      mov eax, [last_task_simd]
      mov cr3, eax
      ; guardo contexto de la ultima tarea
      fxsave [TSS_TASK_0 + tss_t.r_simd]
      ; vuelvo al cr3 actual
      mov cr3, ebx
      ; restauro registros
      fxrstor [TSS_TASK_0 + tss_t.r_simd]

   exit_isr7:
      ; actualizo la ultima tarea en usar simd
      mov [last_task_simd], ebx

      popad
      iret
   

; ------------------------------------------------------------
; ------------------------------------------------------------




isr8_handler_DF:
   mov edx, 8
   jmp default_isr




isr10_handler_TS:
   mov edx, 10
   jmp default_isr




isr11_handler_NP:
   mov edx, 11
   jmp default_isr




isr12_handler_SS:
   mov edx, 12
   jmp default_isr




isr13_handler_GP:
   mov edx, 13
   jmp default_isr




; ------------------------------------------------------------
; ------------------------------------------------------------
isr14_handler_PF:
   pushad
   mov edx, 14

   ; ---------------------------------------------------------
   ; --- Stack frame de Page Fault:
   ; ---------------------------------------------------------
   ; ERROR_CODE <- esp actual
   ; eip
   ; cs
   ; eflags
   ; <- esp anterior
   ;
   ; ---------------------------------------------------------
   ; --- ERROR_CODE
   ; ---------------------------------------------------------
   ; Source: https://wiki.osdev.org/Page_Fault
   ;
   ; 4   I/D   opcode fetch ¿?
   ; 3   RSVD  bits reservados ¿?
   ; 2   U/S   Privilegios incorrectos
   ; 1   R/W   User intenta escribir en Read Only
   ; 0   P     Si el bit P (presente) del page_attr esta en 0
   ; 
   ; ---------------------------------------------------------
   ; --- PAGINA NO CREADA
   ; ---------------------------------------------------------
   ; page_attr = 0x00
   ;  7   PS
   ;  5   NO ACCEDIDO
   ;  4   PCD
   ;  3   PWT
   ;  2   PRIVILEGIO KERNEL
   ;  1   READ
   ;  0   NO PRESENTE
   ;
   ; ---------------------------------------------------------

   ; Leer ERROR_CODE del stack (esta 8 posiciones mas abajo)
;   mov eax, [esp + 32]

   ; Verifico ERROR_CODE
;  si corresponde

   ; Creo pagina pedida
   .createpage:
      push ebp
      mov ebp, esp

      push 3                     ; page_attr
      push 3                     ; table_attr
      mov edx, cr2
      push edx                   ; addr_lin
      push dword [page_addr_phy] ; addr_phy
      mov eax, cr3
      push eax                   ; ptree_entry
      call ptree_entry
      leave

   ; Incremento la pagina fisica entregada
   add dword [page_addr_phy], 0x00001000

   ; Reviso valor de retorno
   cmp eax, 0
   jne .returnerror

   ; Vuelvo a la funcion
   popad
   add esp, 4     ; extraigo ERROR_CODE
   iret

   .returnerror:
      jmp default_isr
; ------------------------------------------------------------
; ------------------------------------------------------------




isr16_handler_MF:
   mov edx, 16
   jmp default_isr




isr17_handler_AC:
   mov edx, 17
   jmp default_isr




isr18_handler_MC:
   mov edx, 18
   jmp default_isr




isr19_handler_XF:
   mov edx, 19
   jmp default_isr




; ------------------------------------------------------------
; ------------------------------------------------------------
%if (ISR32_IS == A_COUNTER)
   GLOBAL isr32_handler_PIT

   ; Interrupcion de PIT
   isr32_handler_PIT:
      pushad

      .count50ms:
         inc byte [timer_count]

      .eoi:
         ; Bajar flag de interrupcion -> EOI al PIC1 (0x20)
         mov al, 0x20
         out 0x20, al
      
      popad

      iret
%endif
; ------------------------------------------------------------
; ------------------------------------------------------------



EXTERN CS_SEL_0
EXTERN DS_SEL_0
EXTERN CS_SEL_3
EXTERN DS_SEL_3
; ------------------------------------------------------------
; ------------------------------------------------------------
; Interrupcion de teclado
isr33_handler_KEYBOARD:
;BOCHS_STOP
   push eax

   ; Leer DATA_PORT
   in al, PS2_DPORT

   ; Si es un codigo release
   bt eax, 0x07           ; Guarda el bit 7 de eax en CF flag
   jc release_code        ; Si CF = 1 => codigo release

%if (KEYBOARD_IS == ON)
   pushad
;   mov eax, DS_SEL_0
;   mov ds, eax
;   mov es, eax

   ; Funcion filtrado tecla + guardado
   push eax
   call alfabeto_h
   pop eax

   ; Compruebo escritura
   push eax
   call read_table_h
   pop eax

   popad 
%endif

   release_code:
      ; Bajar flag de interrupcion -> EOI al PIC1 (0x20)
      mov al, 0x20
      out 0x20, al

      pop eax

      iret
; ------------------------------------------------------------
; ------------------------------------------------------------







