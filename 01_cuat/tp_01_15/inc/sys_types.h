/** 
   \file  sys_types.h   
   \version 01.00
   \brief Biblioteca de declaraciones y definiciones relacionadas con el SO.

   \author Christian Nigri <cnigri@frba.utn.edu.ar>
   \date    01/02/2012
*/
#ifndef _SYS_TYPES_H_
   #define _SYS_TYPES_H_


   /** \def BYTE */
   typedef unsigned char byte;
   /** \def WORD */
   typedef unsigned short word;
   /** \def DWORD */
   typedef unsigned long dword;
   /** \def QWORD */
   typedef unsigned long long qword;


   /* attribute */
   #define __DATA__           __attribute__(( section(".datos")))
   #define __BSS__            __attribute__(( section(".bss")))
   #define __FUNCTIONS__      __attribute__(( section(".functions")))
   #define __TABLA_DIGITOS__  __attribute__(( section(".tabla_digitos")))
   #define __UTILS32__        __attribute__(( section(".utils32")))
   #define __UTILS32_RAM__    __attribute__(( section(".utils32_RAM")))

   #define __T0_TEXT__        __attribute__(( section(".TEXT_t0")))
   #define __T0_BSS__         __attribute__(( section(".bss")))
   #define __T1_TEXT__        __attribute__(( section(".TEXT_t1")))
   #define __T1_BSS__         __attribute__(( section(".bss")))
   #define __T2_TEXT__        __attribute__(( section(".TEXT_t2")))
   #define __T2_BSS__         __attribute__(( section(".bss")))


   /* breakpoint */
   #define BOCHS_STOP         asm("xchg %%bx,%%bx"::);


   /* configs */
   // Configura el comportamiento de las tareas
      #define OFF    0
      #define ON     1
      #define DUMMY  2

         #define TASK01_IS       ON
         #define TASK02_IS       ON


   /* system calls */
   void td3_halt(void);
   void td3_read(void *buffer, dword num_bytes);


   /* simd */
   dword t1_simd(void);
   dword t2_simd(void);


#endif  /*_SYS_TYPES_H_*/




// Debugger stuff
/*
   BOCHS_STOP
      asm("mov %0,%%eax"::"r"(variable));
      asm("nop"::);
   BOCHS_STOP
*/
/*
   if(i==150)
   {
      BOCHS_STOP
      asm("mov %0,%%eax"::"r"(pointer_DTP));

   }
*/



