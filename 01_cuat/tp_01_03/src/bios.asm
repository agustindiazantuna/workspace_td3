; -------------------------------------------------------------------------------
; --- Includes
; -------------------------------------------------------------------------------
%include "processor-flags.h"








; -------------------------------------------------------------------------------
; --- Directivas
; -------------------------------------------------------------------------------

USE16       ; Le indicamos al codigo que trabaje en modo 16bits
				; Los registros ocupan 2 bytes en stack








; -------------------------------------------------------------------------------
; --- Defines
; -------------------------------------------------------------------------------

%define ROM_SIZE        (64*1024)
%define ROM_START       0xf0000
%define RESET_VECTOR    0xffff0

%define DESTINO_1_es    0x0000         ; es
%define DESTINO_1_di    0x0000         ; di

%define ORIGEN_2        0x00000000
%define DESTINO_2_es    0x0000         ; es
%define DESTINO_2_di    0x00300000     ; di







; -------------------------------------------------------------------------------
; --- Start16
; -------------------------------------------------------------------------------

start16:	
   test eax, 0x0              ; Verificar que el uP no este en fallo
   jne ..@fault_end

   xor eax, eax
   mov cr3, eax               ; Invalidar TLB
 
   ; Habilito A20
   jmp A20_Enable_No_Stack
   A20_Enable_No_Stack_return:

   ; -------------------------------------------------------
   ; punto i
   ; -------------------------------------------------------
   ; td3_memcopy
   mov di, DESTINO_1_di       ;  1er arg - DESTINO
   mov si, ROM_START          ;  2do arg - ORIGEN
   mov cx, ROM_SIZE-1         ;  3er arg - CANTIDAD

   lazo:
      mov ax,[cs:si]
      mov [di],ax
      add di,2
      add si,2
   loop lazo

   ; Salto a la primer copia
   jmp 0x0000:afterjmp_1
   afterjmp_1:

   ; -------------------------------------------------------
   ; Para moverme por los 4GB cargo GDT
   ; -------------------------------------------------------
   o32 lgdt  [cs:gdtr]
   ;->Establecer el up en MP<-
   smsw  ax
   or    ax, X86_CR0_PE
   lmsw  ax

   mov ax, DS_SEL
   mov ds, ax

   ;->Quitar el up de MP<-
   mov  eax, cr0
   and  eax, 0xfffffffe
   mov  cr0, eax

   ; -------------------------------------------------------
   ; punto ii
   ; -------------------------------------------------------
   ; td3_memcopy
   mov edi, DESTINO_2_di      ;  1er arg - DESTINO
   mov esi, ORIGEN_2          ;  2do arg - ORIGEN
   mov ecx, ROM_SIZE-1        ;  3er arg - CANTIDAD

   lazo2:
      mov ax,[cs:si]
      mov [edi],ax
      add edi,2
      add si,2
   loop lazo2
%if 0
   mov eax, DESTINO_2_di
   add eax, afterjmp_2

   ; Salto a la segunda copia
   jmp eax
   afterjmp_2:
%endif
   ; Defino registros del stack --> ss:sp --> 000F:FFFF
   ; Establecer la pila en 0x1FFFB000 
   mov eax, 0x1FFF;0x000F;0xF000
   mov ss, eax

   mov eax, 0xB000;0xFFFF
   mov esp, eax

   hlt








; -------------------------------------------------------------------------------
; --- GDT
; -------------------------------------------------------------------------------

GDT:
NULL_SEL    equ $-GDT
   dq 0x0
DS_SEL      equ $-GDT
   dw 0xffff 
   dw 0x0000
   db 0x00
   db 0x92
   db 0xcf
   db 0
GDT_LENGTH  equ $-GDT

gdtr:
   dw GDT_LENGTH-1
   dd GDT








; -------------------------------------------------------------------------------
; --- Fault_end
; -------------------------------------------------------------------------------

..@fault_end:
   hlt
   jmp ..@fault_end








;///////////////////////////////////////////////////////////////////////////////
;                   Funciones para habilitar el A20 Gate
;///////////////////////////////////////////////////////////////////////////////
%define     PORT_A_8042    0x60        ;Puerto A de E/S del 8042
%define     CTRL_PORT_8042 0x64        ;Puerto de Estado del 8042
%define     KEYB_DIS       0xAD        ;Deshabilita teclado con Command Byte
%define     KEYB_EN        0xAE        ;Habilita teclado con Command Byte
%define     READ_OUT_8042  0xD0        ;Copia en 0x60 el estado de OUT
%define     WRITE_OUT_8042 0xD1        ;Escribe en OUT lo almacenado en 0x60

USE16
;------------------------------------------------------------------------------
;| Título: A20_Enable_No_Stack                                                |
;| Versión:       1.0                     Fecha:   26/02/2018                 |
;| Autor:         ChristiaN               Modelo:  IA-32 (16bits)             |
;| ------------------------------------------------------------------------   |
;| Descripción:                                                               |
;|    Habilita la puerta A20 sin utilizacion de la pila.                      |
;|    Referencia https://wiki.osdev.org/A20_Line                              |
;| ------------------------------------------------------------------------   |
;| Recibe:                                                                    |
;|    Nada                                                                    |
;|                                                                            |
;| Retorna:                                                                   |
;|    Nada                                                                    |
;| ------------------------------------------------------------------------   |
;| Revisiones:                                                                |
;|    1.0 | 26/02/2018 | ChristiaN | Original                                 |
;------------------------------------------------------------------------------
A20_Enable_No_Stack:

   xor ax, ax
   ;Deshabilita el teclado
   mov di, .8042_kbrd_dis
   jmp .empty_8042_in
   .8042_kbrd_dis:
   mov al, KEYB_DIS
   out CTRL_PORT_8042, al
 
   ;Lee la salida
   mov di, .8042_read_out
   jmp .empty_8042_in
   .8042_read_out:
   mov al, READ_OUT_8042
   out CTRL_PORT_8042, al
   
   .empty_8042_out:  
;      in al, CTRL_PORT_8042      ; Lee port de estado del 8042 hasta que el
;      test al, 00000001b         ; buffer de salida este vacio
;      jne .empty_8042_out

   xor bx, bx   
   in al, PORT_A_8042
   mov bx, ax

   ;Modifica el valor del A20
   mov di, .8042_write_out
   jmp .empty_8042_in
   .8042_write_out:
   mov al, WRITE_OUT_8042
   out CTRL_PORT_8042, al

   mov di, .8042_set_a20
   jmp .empty_8042_in
   .8042_set_a20:
   mov ax, bx
   or ax, 00000010b              ; Habilita el bit A20
   out PORT_A_8042, al

   ;Habilita el teclado
   mov di, .8042_kbrd_en
   jmp .empty_8042_in
   .8042_kbrd_en:
   mov al, KEYB_EN
   out CTRL_PORT_8042, al

   mov di, .a20_enable_no_stack_exit
   .empty_8042_in:  
;      in al, CTRL_PORT_8042      ; Lee port de estado del 8042 hasta que el
;      test al, 00000010b         ; buffer de entrada este vacio
;      jne .empty_8042_in
      jmp di

   .a20_enable_no_stack_exit:

jmp A20_Enable_No_Stack_return








; -------------------------------------------------------------------------------
; --- El codigo arranca en 0xFFFF0
; -------------------------------------------------------------------------------

   CODE_LENGTH equ ($-start16)

   ; ocupa espacio con nop para posicionar el codigo en 0xFFFF0
   times (RESET_VECTOR - ROM_START - CODE_LENGTH) nop	

reset_vector:
   ; Limpio flags
   cli	     		; deshabilito las interrupciones
   cld             ; Clear Direction Flag: DF = 0 -> Los contadores se incrementan

   ; El procesador arranca en 0xFFFF0 y pasamos a 0x00000 (comienzo de la ROM)
   jmp start16

   ; Alineo para que la ROM ocupe 64k
   align 16		; Alinea a 16 bytes, en este caso cae a 65531







