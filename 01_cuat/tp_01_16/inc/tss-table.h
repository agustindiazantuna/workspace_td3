; ------------------------------------------------------------------------------
; --- struc
; ------------------------------------------------------------------------------

; Definicion de la estructura denominada tss_t
struc   tss_t
   .previous_task_link:    resw 1
   .reserved0:             resw 1
   .r_esp0:                resd 1
   .r_ss0:                 resw 1
   .reserved1:             resw 1
   .r_esp1:                resd 1
   .r_ss1:                 resw 1
   .reserved2:             resw 1
   .r_esp2:                resd 1
   .r_ss2:                 resw 1
   .reserved3:             resw 1
   .r_cr3:                 resd 1
   .r_eip:                 resd 1
   .r_eflags:              resd 1
   .r_eax:                 resd 1
   .r_ecx:                 resd 1
   .r_edx:                 resd 1
   .r_ebx:                 resd 1
   .r_esp:                 resd 1
   .r_ebp:                 resd 1
   .r_esi:                 resd 1
   .r_edi:                 resd 1
   .r_es:                  resw 1
   .reserved4:             resw 1
   .r_cs:                  resw 1
   .reserved5:             resw 1
   .r_ss:                  resw 1
   .reserved6:             resw 1
   .r_ds:                  resw 1
   .reserved7:             resw 1
   .r_fs:                  resw 1
   .reserved8:             resw 1
   .r_gs:                  resw 1
   .reserved9:             resw 1
   .LTD_ss:                resw 1
   .reserved10:            resw 1
   .reserved11:            resw 1
   .io_map_base_addr:      resw 1
   .void_to_align_16       resb 8
   .r_simd                 resb 512
endstruc



