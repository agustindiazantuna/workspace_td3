;-------------------------------------------------------------------------------
;|  Título:         Data                                                       |
;|  Versión:        1.0                     Fecha:  18/06/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Contiene variables:                                                    |
;|          - No inicializadas (.bss section)                                  |
;|          - Inicializadas (.datos section)                                   |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 18/06/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL tabla_digitos_count
GLOBAL timer_count
GLOBAL last_count
GLOBAL page_addr_phy

GLOBAL current_task
GLOBAL next_task
GLOBAL count_t1
GLOBAL count_t2
GLOBAL count_t3
GLOBAL count_t4

GLOBAL last_task_simd


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss
; ------------------------------------------------------------------------------

; Variables del keyboard
   tabla_digitos_count:
      resd 1


; Variables de timer, contadores hasta 10 (500ms)
   timer_count:
      resb 1         ; Reservo 1 byte

   last_count:
      resb 1         ; Reservo 1 byte


; Variables de scheduler
   current_task:
      resb 1         ; Reservo 1 byte

   next_task:
      resb 1         ; Reservo 1 byte


; Variables de simd
   last_task_simd:
      resd 1


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .datos progbits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss
; ------------------------------------------------------------------------------

; Variables de ISR14: PAGE FAULT
   page_addr_phy:
      dd 0x10000000    ; Reservo 1 dword


; Variables de scheduler
; Secuencia de tareas:
;  00001-00021-00031-00421-0001-...
   count_t1:
      db 0x00          ; Defino byte = 0

   count_t2:
      db 0x01          ; Defino byte = 1

   count_t3:
      db 0x01          ; Defino byte = 1

   count_t4:
      db 0x02          ; Defino byte = 1







