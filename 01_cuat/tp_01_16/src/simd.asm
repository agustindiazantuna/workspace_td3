;-------------------------------------------------------------------------------
;|  Título:         SIMD                                                       |
;|  Versión:        1.0                     Fecha:  25/08/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Contiene:                                                              |
;|          - APIs de acceso publico para acceder a las System Calls           |
;|          - Handler de la IRQ 0x80                                           |
;|          - Rutinas de las System Calls ejecutadas por el kernel             |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 25/08/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx
%define num_qword             20


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN tabla_digitos_t1
EXTERN tabla_digitos_t2
EXTERN tabla_digitos_t3
EXTERN tabla_digitos_t4


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL t1_simd
GLOBAL t2_simd
GLOBAL t3_simd
GLOBAL t4_simd


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss_t1 nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss_t1
; ------------------------------------------------------------------------------

   acumulado_t1:
      resd 1


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .TEXT_t1 progbits


; ------------------------------------------------------------------------------
; --- t1_simd : Suma aritmetica     "desborde"     "dword"
; ------------------------------------------------------------------------------
t1_simd:
   pushad

   ; ebx contiene la cantidad de loops: 128 xmm / 64 mem = 2
   mov ebx, num_qword/2
   ; ecx es el indice dentro del vector
   xor ecx, ecx
   mov [acumulado_t1], ecx

   .lazofor:
      mov eax, 16
      mul ecx

      movdqu xmm0, [tabla_digitos_t1 + eax]
      
      ; para realizar la suma horizontal de 4 dword en el mismo registro
      phaddd xmm0, xmm0
      phaddd xmm0, xmm0

      ; obtengo el resultado en eax
      movd eax, xmm0
      add [acumulado_t1], eax

      inc ecx
      cmp ecx, ebx
      jne .lazofor

   popad
   mov eax, [acumulado_t1]
   ret












; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss_t2 nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss_t2
; ------------------------------------------------------------------------------

   acumulado_t2:
      resb 1

   auxiliar_t2:
      resq 2


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .TEXT_t2 progbits


; ------------------------------------------------------------------------------
; --- t2_simd : Suma aritmetica     "desborde"     "byte"
; ------------------------------------------------------------------------------
t2_simd:
   pushad

   ; ebx contiene la cantidad de loops: 64 mem / byte = 2
   mov ebx, num_qword/2
   ; ecx es el indice dentro del vector
   xor ecx, ecx
   mov [acumulado_t2], ecx
   ; limpio xmm1
   xorps xmm1, xmm1

   ; levanto la tabla de digitos de a 16 bytes
   .lazofor:
      mov eax, 16
      mul ecx

      movdqu xmm0, [tabla_digitos_t2 + eax]
      
      ; suma la tabla acumulando el resultado en xmm1
      paddb xmm1, xmm0

      inc ecx
      cmp ecx, ebx
      jne .lazofor


   movdqu [auxiliar_t2], xmm1
   mov ebx, 16
   xor ecx, ecx
   xor eax, eax

   .lazo_acumulado:
      ; suma el acumulado en al
      add byte al, [auxiliar_t2 + ecx]

      inc ecx
      cmp ecx, ebx
      jne .lazo_acumulado


   ; obtengo el resultado en al
   mov [acumulado_t2], al

   popad
   xor eax, eax
   mov al, [acumulado_t2]
   ret












; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss_t3 nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss_t3
; ------------------------------------------------------------------------------

   acumulado_t3:
      resb 1

   auxiliar_t3:
      resq 2


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .TEXT_t3 progbits


; ------------------------------------------------------------------------------
; --- t3_simd : Suma aritmetica     "saturado"  "signado"   "byte"
; ------------------------------------------------------------------------------
t3_simd:
   pushad

   ; ebx contiene la cantidad de loops: 64 mem / byte = 2
   mov ebx, num_qword/2
   ; ecx es el indice dentro del vector
   xor ecx, ecx
   mov [acumulado_t3], ecx
   ; limpio xmm1
   xorps xmm1, xmm1

   ; levanto la tabla de digitos de a 16 bytes
   .lazofor:
      mov eax, 16
      mul ecx

      movdqu xmm0, [tabla_digitos_t3 + eax]
      
      ; suma la tabla acumulando el resultado en xmm1
      paddsb xmm1, xmm0

      inc ecx
      cmp ecx, ebx
      jne .lazofor


   movdqu [auxiliar_t3], xmm1
   mov ebx, 16
   xor ecx, ecx
   xor eax, eax

   .lazo_acumulado:
      ; suma el acumulado en al
      add byte al, [auxiliar_t3 + ecx]

      inc ecx
      cmp ecx, ebx
      jne .lazo_acumulado


   ; obtengo el resultado en al
   mov [acumulado_t3], al

   popad
   xor eax, eax
   mov al, [acumulado_t3]
   ret












; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss_t4 nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss_t4
; ------------------------------------------------------------------------------

   acumulado_t4:
      resb 1

   auxiliar_t4:
      resq 2


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .TEXT_t4 progbits


; ------------------------------------------------------------------------------
; --- t4_simd : Suma aritmetica     "saturado"  "no signado"   "byte"
; ------------------------------------------------------------------------------
t4_simd:
   pushad

   ; ebx contiene la cantidad de loops: 64 mem / byte = 2
   mov ebx, num_qword/2
   ; ecx es el indice dentro del vector
   xor ecx, ecx
   mov [acumulado_t4], ecx
   ; limpio xmm1
   xorps xmm1, xmm1

   ; levanto la tabla de digitos de a 16 bytes
   .lazofor:
      mov eax, 16
      mul ecx

      movdqu xmm0, [tabla_digitos_t4 + eax]
      
      ; suma la tabla acumulando el resultado en xmm1
      paddusb xmm1, xmm0

      inc ecx
      cmp ecx, ebx
      jne .lazofor


   movdqu [auxiliar_t4], xmm1
   mov ebx, 16
   xor ecx, ecx
   xor eax, eax

   .lazo_acumulado:
      ; suma el acumulado en al
      add byte al, [auxiliar_t4 + ecx]

      inc ecx
      cmp ecx, ebx
      jne .lazo_acumulado


   ; obtengo el resultado en al
   mov [acumulado_t4], al

   popad
   xor eax, eax
   mov al, [acumulado_t4]
   ret







