/** 
   \file    task4.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 4

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */
#define num_qword    10
#define num_bytes    num_qword*4


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */

// Variables local de la tarea 4
dword suma_tabla_digitos_t4 __T4_BSS__ = 0;
qword tabla_digitos_t4[20] __T4_BSS__ = {0};


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__T4_TEXT__ void task04(void)
{
   #if (TASK04_IS == DUMMY)

      while(1)
      {
//         BOCHS_STOP
         asm("mov %0,%%eax"::"r"(0xFFFF0004));
         td3_halt();
      }

   #elif (TASK04_IS == ON)

      while(1)
      {  
         if(suma_tabla_digitos_t4 == 0x7000)
         {
            BOCHS_STOP
            asm("mov %0,%%eax"::"r"(0xFFFF0004));
            asm("mov %0,%%eax"::"r"(suma_tabla_digitos_t4));
         }
   
         td3_read((void *) tabla_digitos_t4, num_bytes);


         suma_tabla_digitos_t4 = t4_simd();


         td3_halt();
      }

   #endif

}







