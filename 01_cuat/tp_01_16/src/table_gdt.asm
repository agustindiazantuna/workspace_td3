;-------------------------------------------------------------------------------
;|  Título:         Table GDT                                                  |
;|  Versión:        1.0                     Fecha:  16/06/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Tablas GDT                                                             |
;|          - En ROM                                                           |
;|          - En RAM                                                           |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 16/06/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN _TSS_base_00_15
EXTERN _TSS_base_16_23
EXTERN _TSS_base_23_31


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL gdt_to_gdtr
GLOBAL CS_SEL_0
GLOBAL DS_SEL_0
GLOBAL CS_SEL_3
GLOBAL DS_SEL_3
GLOBAL TSS
GLOBAL gdt_to_gdtr_ROM
GLOBAL CS_SEL_0_ROM
GLOBAL DS_SEL_0_ROM


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
; progbits : datos definidos
; nobits : datos no inicializados
SECTION .gdt_table progbits alloc noexec nowrite


; ------------------------------------------------------------------------------
; --- struc
; ------------------------------------------------------------------------------
struc   gdtd_t                    ; Definicion de la estructura denominada gdtd_t
        .lim_00_15:     resw 1    ; Limite del segmento 00-15
        .base00_15:     resw 1    ; Direccion base del segmento bits 00-15
        .base16_23:     resb 1    ; Direccion base del segmento bits 16-23
        .prop:          resb 1    ; Propiedades
        .lim_prop:      resb 1    ; Limite del segmento 16-19 y propiedades
        .base24_31:     resb 1    ; Direccion base del segmento bits 24-31
endstruc


; ------------------------------------------------------------------------------
; --- GDT
; ------------------------------------------------------------------------------
; SEL_0 : Maximo nivel de privilegio -> kernel
; SEL_3 : Minimo nivel de privilegio -> user
;
; ------------------------------------------------------------------------------
; Atributos:
;
;  BITS  NAME  DESCRIPTION
;  23    G     Granularity    0: offset de byte // 1: de 4 KB
;  22    D/B   Default/Big    0: segmento de 16 bits // 1: de 32 bits
;  21    L     Long mode seg  0: codigo de 32 bits // 1: de 64 bits
;  20    AVL   Available
;
;  15    P     Present.        0: no presente en RAM // 1: presente en RAM
;  14-3  DPL   Desc Priv Level 00: mayor // 11: menor
;  12    S     System.         0: es de sistema // 1: no es de sistema
;
;        S = 1   | S = 0
;  11    T       | 0: datos            | 1: codigo
;  10    Y       | 1: expand down      | 1: conforming, se accede con call desde
;                |                     |    un segmento con menor DPL
;  09    P       | 0: R only // 1: R/W | 0: X only // 1: X/R
;  08    E       | Accedido.
; ------------------------------------------------------------------------------
; 
; 0x9A : P - 00 - no system // code - no conforming - X/R - no accedido
; 0x92 : P - 00 - no system // data - no expand down - R/W - no accedido
;
; 0xFA : P - 11 - no system // code - no conforming - X/R - no accedido
; 0xF2 : P - 11 - no system // data - no expand down - R/W - no accedido
;
; 0x89 : P - 00 - system // tss 32 bits: 1001
;
;
; Source:
;        https://en.wikipedia.org/wiki/Segment_descriptor
;        https://wiki.osdev.org/Descriptor
;


GDT:
  NULL_SEL    equ $-GDT
     dq 0x0

  CS_SEL_0    equ $-GDT + 0x00              ; Define 8
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xFFFF
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 10011010b   ; 0x9A
        at gdtd_t.lim_prop,  db 11001111b   ; prop: 0xC - lim: 0xF
        at gdtd_t.base24_31, db 0
     iend

  DS_SEL_0    equ $-GDT + 0x00              ; Define 16
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xFFFF
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 10010010b   ; 0x92
        at gdtd_t.lim_prop,  db 11001111b   ; prop: 0xC - lim: 0xF
        at gdtd_t.base24_31, db 0
     iend

  CS_SEL_3    equ $-GDT + 0x03
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xFFFF
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 11111010b   ; 0xFA
        at gdtd_t.lim_prop,  db 11001111b   ; prop: 0xC - lim: 0xF
        at gdtd_t.base24_31, db 0
     iend

  DS_SEL_3    equ $-GDT + 0x03
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xFFFF
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 11110010b   ; 0xF2
        at gdtd_t.lim_prop,  db 11001111b   ; prop: 0xC - lim: 0xF
        at gdtd_t.base24_31, db 0
     iend

  TSS       equ $-GDT
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw _TSS_base_23_31
        at gdtd_t.base00_15, dw _TSS_base_00_15
        at gdtd_t.base16_23, db _TSS_base_16_23
        at gdtd_t.prop,      db 10001001b   ; 0x89
        at gdtd_t.lim_prop,  db 11000000b   ; prop: 0xC - lim: 0x0
        at gdtd_t.base24_31, db 0x00
     iend

  GDT_LENGTH  equ $-GDT

gdt_to_gdtr:
  dw GDT_LENGTH-1            ; Tamaño de la tabla en reg 16 bits
  dd GDT                     ; Dirección relativa en reg 32 bits


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
; progbits : datos definidos
; nobits : datos no inicializados
SECTION .gdt_table_ROM progbits alloc noexec nowrite


; ------------------------------------------------------------------------------
; --- GDT_ROM
; ------------------------------------------------------------------------------
; SEL_0 : Maximo nivel de privilegio -> kernel
; SEL_3 : Minimo nivel de privilegio -> user

GDT_ROM:
  NULL_SEL_ROM    equ $-GDT_ROM
     dq 0x0
  CS_SEL_0_ROM    equ $-GDT_ROM                     ; Define 8
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xffff
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 10011010b   ; 0x9A
        at gdtd_t.lim_prop,  db 11001111b   ; prop: 0xC - lim: 0xF
        at gdtd_t.base24_31, db 0
     iend
  DS_SEL_0_ROM    equ $-GDT_ROM                     ; Define 16
     istruc gdtd_t
        at gdtd_t.lim_00_15, dw 0xffff
        at gdtd_t.base00_15, dw 0x0000
        at gdtd_t.base16_23, db 0x00
        at gdtd_t.prop,      db 10010010b   ; 0x92
        at gdtd_t.lim_prop,  db 11001111b   ; prop: 0xC - lim: 0xF
        at gdtd_t.base24_31, db 0
     iend
  GDT_LENGTH_ROM  equ $-GDT_ROM                     ; Define 24

gdt_to_gdtr_ROM:
  dw GDT_LENGTH_ROM-1            ; Tamaño de la tabla en reg 16 bits
  dd GDT_ROM                     ; Dirección relativa en reg 32 bits






