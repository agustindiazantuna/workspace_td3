/** 
   \file    task3.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 3

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */
#define num_qword    10
#define num_bytes    num_qword*4


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */

// Variables local de la tarea 3
dword suma_tabla_digitos_t3 __T3_BSS__ = 0;
qword tabla_digitos_t3[20] __T3_BSS__ = {0};


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__T3_TEXT__ void task03(void)
{
   #if (TASK03_IS == DUMMY)

      while(1)
      {
//         BOCHS_STOP
         asm("mov %0,%%eax"::"r"(0xFFFF0003));
         td3_halt();
      }

   #elif (TASK03_IS == ON)

      while(1)
      {  
         if(suma_tabla_digitos_t3 == 0x6000)
         {
            BOCHS_STOP
            asm("mov %0,%%eax"::"r"(0xFFFF0003));
            asm("mov %0,%%eax"::"r"(suma_tabla_digitos_t3));
         }
   
         td3_read((void *) tabla_digitos_t3, num_bytes);


         suma_tabla_digitos_t3 = t3_simd();


         td3_halt();
      }

   #endif

}







