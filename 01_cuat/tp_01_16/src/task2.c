/** 
   \file    task2.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 2

   \author  Agustin Diaz Antuna
   \date    29/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */
#define num_qword    10
#define num_bytes    num_qword*4


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */

// Variables local de la tarea 2
dword suma_tabla_digitos_t2 __T2_BSS__ = 0;
qword tabla_digitos_t2[20] __T2_BSS__ = {0};


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__T2_TEXT__ void task02(void)
{
   #if (TASK02_IS == DUMMY)

      while(1)
      {
//         BOCHS_STOP
         asm("mov %0,%%eax"::"r"(0xFFFF0002));
         td3_halt();
      }

   #elif (TASK02_IS == ON)

      while(1)
      {
         if(suma_tabla_digitos_t2 == 0x5000)
         {
            BOCHS_STOP
            asm("mov %0,%%eax"::"r"(0xFFFF0002));
            asm("mov %0,%%eax"::"r"(suma_tabla_digitos_t2));
         }
   
         td3_read((void *) tabla_digitos_t2, num_bytes);


         suma_tabla_digitos_t2 = t2_simd();


         td3_halt();
      }

   #endif

}







