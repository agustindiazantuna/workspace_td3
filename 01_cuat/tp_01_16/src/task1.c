/** 
   \file    task1.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 1

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */
#define num_qword    10
#define num_bytes    num_qword*4


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */

// Variables local de la tarea 1
dword suma_tabla_digitos_t1 __T1_BSS__ = 0;
qword tabla_digitos_t1[20] __T1_BSS__ = {0};


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__T1_TEXT__ void task01(void)
{
   #if (TASK01_IS == DUMMY)

      while(1)
      {
//         BOCHS_STOP
         asm("mov %0,%%eax"::"r"(0xFFFF0001));
         td3_halt();
      }

   #elif (TASK01_IS == ON)

      while(1)
      {  
         if(suma_tabla_digitos_t1 == 0x4000)
         {
            BOCHS_STOP
            asm("mov %0,%%eax"::"r"(0xFFFF0001));
            asm("mov %0,%%eax"::"r"(suma_tabla_digitos_t1));
         }
   
         td3_read((void *) tabla_digitos_t1, num_bytes);

         
         suma_tabla_digitos_t1 = t1_simd();


         td3_halt();
      }

   #endif

}







