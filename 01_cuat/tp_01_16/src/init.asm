;-------------------------------------------------------------------------------
;|  Título:         Initialize 16 - 32                                         |
;|  Versión:        1.0                     Fecha:  16/06/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Inicializacion 16 bits:                                                |
;|          - Habilita A20                                                     |
;|          - Carga GDT (de ROM)                                               |
;|          - Habilita MP                                                      |
;|      Inicializacion 32 bits:                                                |
;|          - Carga selectores                                                 |
;|          - Shadow copy                                                      |
;|          - Carga GDT e IDT (de RAM)                                         |
;|          - Shadow copy                                                      |
;|          - Carga arbol de paginacion                                        |
;|          - Configura interrupciones (PIT)                                   |
;|          - Habilta instrucciones SIMD                                       |
;|          - Habilita paginacion                                              |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 16/06/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE16


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "processor-flags.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN A20_Enable_No_Stack
EXTERN gdt_to_gdtr_ROM
EXTERN CS_SEL_0_ROM


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL _init16
GLOBAL A20_Enable_No_Stack_return


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start16 progbits


; ------------------------------------------------------------------------------
; --- _init16
; ------------------------------------------------------------------------------
_init16:
   ; Verificar que el uP no este en fallo
   test  eax, 0x00
   jne   ..@fault_end


   ; Habilito A20
   jmp A20_Enable_No_Stack
   A20_Enable_No_Stack_return:


   ; Cargo GDT
   o32 lgdt [cs:gdt_to_gdtr_ROM]


   ; Establecer el uP en MP
   smsw  ax
   or    ax, X86_CR0_PE
   lmsw  ax


   o32 jmp dword CS_SEL_0_ROM:_init32




   ; Error
   ..@fault_end:
      hlt
      jmp ..@fault_end








; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN DS_SEL_0_ROM
EXTERN _STACK_END
EXTERN _STACK_SIZE
EXTERN td3_memcopy

EXTERN __isq_size
EXTERN __isq_lma
EXTERN __isq_lin
EXTERN __systemcall_nivel3_size
EXTERN __systemcall_nivel3_lma
EXTERN __systemcall_nivel3_lin
EXTERN __systables_size
EXTERN __systables_lma
EXTERN __systables_lin
EXTERN __kernel_size
EXTERN __kernel_lin
EXTERN __kernel_lma
EXTERN __TEXT_t0_size
EXTERN __TEXT_t0_lma
EXTERN __TEXT_t0_phy
EXTERN __TEXT_t1_size
EXTERN __TEXT_t1_lma
EXTERN __TEXT_t1_phy
EXTERN __TEXT_t2_size
EXTERN __TEXT_t2_lma
EXTERN __TEXT_t2_phy
EXTERN __TEXT_t3_size
EXTERN __TEXT_t3_lma
EXTERN __TEXT_t3_phy
EXTERN __TEXT_t4_size
EXTERN __TEXT_t4_lma
EXTERN __TEXT_t4_phy
EXTERN __DATA_size
EXTERN __DATA_lma
EXTERN __DATA_lin

EXTERN _STACK_t0_nivel0_START_lma
EXTERN _STACK_t0_nivel0_START_phy 
EXTERN _STACK_t1_nivel0_START_lma
EXTERN _STACK_t1_nivel0_START_phy 
EXTERN _STACK_t2_nivel0_START_lma
EXTERN _STACK_t2_nivel0_START_phy
EXTERN _STACK_t3_nivel0_START_lma
EXTERN _STACK_t3_nivel0_START_phy 
EXTERN _STACK_t4_nivel0_START_lma
EXTERN _STACK_t4_nivel0_START_phy  

EXTERN gdt_to_gdtr
EXTERN idt_to_idtr
EXTERN init_irq
EXTERN init_pagetables_c
EXTERN CS_SEL_0
EXTERN _PAG_TABLE_t0_START
EXTERN kernel


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL init_irq_ret


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start32 progbits


; ------------------------------------------------------------------------------
; --- _init32
; ------------------------------------------------------------------------------
_init32:

   .init_segments:
      ; Inicializar la pila en segmento DS  
      mov ax, DS_SEL_0_ROM
      mov ss, ax
      mov esp, _STACK_END

      ; Limpiar el stack
      xor ebx, ebx
      mov ecx, _STACK_SIZE

      .stack_init:
         push ebx
         loop .stack_init
      mov esp, _STACK_END

      ; Inicializar la selectores datos
      mov ds, ax
      mov es, ax
      mov gs, ax
      mov fs, ax


   .copy_to_ram:
      ; Desempaquetar la ROM
      ; isr
      push ebp
      mov ebp, esp
      push __isq_size            ;  arg3  -> cantidad
      push __isq_lma             ;  arg2  -> origen
      push __isq_lin             ;  arg1  -> destino
      call td3_memcopy
      leave

      ; systemcall_nivel3
      push ebp
      mov ebp, esp
      push __systemcall_nivel3_size ;  arg3  -> cantidad
      push __systemcall_nivel3_lma  ;  arg2  -> origen
      push __systemcall_nivel3_lin  ;  arg1  -> destino
      call td3_memcopy
      leave

      ; systables
      push ebp
      mov ebp, esp
      push __systables_size      ;  arg3  -> cantidad
      push __systables_lma       ;  arg2  -> origen
      push __systables_lin       ;  arg1  -> destino
      call td3_memcopy
      leave

      ; kernel
      push ebp
      mov ebp, esp
      push __kernel_size         ;  arg3  -> cantidad
      push __kernel_lma          ;  arg2  -> origen
      push __kernel_lin          ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TEXT_t0
      push ebp
      mov ebp, esp
      push __TEXT_t0_size        ;  arg3  -> cantidad
      push __TEXT_t0_lma         ;  arg2  -> origen
      push __TEXT_t0_phy         ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TEXT_t1
      push ebp
      mov ebp, esp
      push __TEXT_t1_size        ;  arg3  -> cantidad
      push __TEXT_t1_lma         ;  arg2  -> origen
      push __TEXT_t1_phy         ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TEXT_t2
      push ebp
      mov ebp, esp
      push __TEXT_t2_size        ;  arg3  -> cantidad
      push __TEXT_t2_lma         ;  arg2  -> origen
      push __TEXT_t2_phy         ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TEXT_t3
      push ebp
      mov ebp, esp
      push __TEXT_t3_size        ;  arg3  -> cantidad
      push __TEXT_t3_lma         ;  arg2  -> origen
      push __TEXT_t3_phy         ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TEXT_t4
      push ebp
      mov ebp, esp
      push __TEXT_t4_size        ;  arg3  -> cantidad
      push __TEXT_t4_lma         ;  arg2  -> origen
      push __TEXT_t4_phy         ;  arg1  -> destino
      call td3_memcopy
      leave

      ; DATA
      push ebp
      mov ebp, esp
      push __DATA_size           ;  arg3  -> cantidad
      push __DATA_lma            ;  arg2  -> origen
      push __DATA_lin            ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TSS_t0
      push ebp
      mov ebp, esp
      push 0x00000104                  ;  arg3  -> cantidad
      push _STACK_t0_nivel0_START_lma  ;  arg2  -> origen
      push _STACK_t0_nivel0_START_phy  ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TSS_t1
      push ebp
      mov ebp, esp
      push 0x00000104                  ;  arg3  -> cantidad
      push _STACK_t1_nivel0_START_lma  ;  arg2  -> origen
      push _STACK_t1_nivel0_START_phy  ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TSS_t2
      push ebp
      mov ebp, esp
      push 0x00000104                  ;  arg3  -> cantidad
      push _STACK_t2_nivel0_START_lma  ;  arg2  -> origen
      push _STACK_t2_nivel0_START_phy  ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TSS_t3
      push ebp
      mov ebp, esp
      push 0x00000104                  ;  arg3  -> cantidad
      push _STACK_t3_nivel0_START_lma  ;  arg2  -> origen
      push _STACK_t3_nivel0_START_phy  ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TSS_t4
      push ebp
      mov ebp, esp
      push 0x00000104                  ;  arg3  -> cantidad
      push _STACK_t4_nivel0_START_lma  ;  arg2  -> origen
      push _STACK_t4_nivel0_START_phy  ;  arg1  -> destino
      call td3_memcopy
      leave


   .load_systables:
      ; Cargar GDT ya copiada en RAM
      lgdt [gdt_to_gdtr]
      ; Cargar IDT ya copiada en RAM
      lidt [idt_to_idtr]


   ; Cargar tablas paginacion con rutina en C
   call init_pagetables_c


   ; Inicializar irq
   jmp init_irq
   init_irq_ret:


   ; Habilitar instrucciones SIMD
   .enable_simd:
      mov eax, cr0
      and eax, ~X86_CR0_EM
      or eax, X86_CR0_TS
      mov cr0, eax

      mov eax, cr4
      or eax, X86_CR4_OSFXSR
      mov cr4, eax


   .init_paging:
      ; cargo cr3
      mov eax, _PAG_TABLE_t0_START
      mov cr3, eax


   jmp kernel







