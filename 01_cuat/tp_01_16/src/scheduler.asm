;-------------------------------------------------------------------------------
;|  Título:         Scheduler                                                  |
;|  Versión:        1.0                     Fecha:  12/06/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (32bits)             |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Contiene:                                                              |
;|          - Creacion de espacios de contexto de cada tarea (formato Intel)   |
;|          - Handler de la IRQ 32 - PIT (timer)                               |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 12/06/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "processor-flags.h"
%include "tss-table.h"
%include "config.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx
%define task0        0
%define task1        1
%define task2        2
%define task3        3
%define task4        4
%define DEFAULT_EFLAGS        0x202

%if TASK_TIME == DEBUG
   %define time_task1   5
   %define time_task2   10
   %define time_task3   15
   %define time_task4   20
%else
   %define time_task1   100
   %define time_task2   200
   %define time_task3   300
   %define time_task4   400
%endif


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN CS_SEL_0
EXTERN DS_SEL_0
EXTERN CS_SEL_3
EXTERN DS_SEL_3
EXTERN current_task
EXTERN next_task

EXTERN _PAG_TABLE_t0_START
EXTERN _STACK_t0_nivel0_END_lin
EXTERN __TEXT_t0_lin
EXTERN _STACK_t0_END_lin

EXTERN _PAG_TABLE_t1_START
EXTERN _STACK_t1_nivel0_END_lin
EXTERN __TEXT_t1_lin
EXTERN _STACK_t1_END_lin

EXTERN _PAG_TABLE_t2_START
EXTERN _STACK_t2_nivel0_END_lin
EXTERN __TEXT_t2_lin
EXTERN _STACK_t2_END_lin

EXTERN _PAG_TABLE_t3_START
EXTERN _STACK_t3_nivel0_END_lin
EXTERN __TEXT_t3_lin
EXTERN _STACK_t3_END_lin

EXTERN _PAG_TABLE_t4_START
EXTERN _STACK_t4_nivel0_END_lin
EXTERN __TEXT_t4_lin
EXTERN _STACK_t4_END_lin

EXTERN count_t1
EXTERN count_t2
EXTERN count_t3
EXTERN count_t4


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL TSS_TASK_0


; ------------------------------------------------------------------------------
; --- tss
; ------------------------------------------------------------------------------
SECTION .tss_task_0 progbits

  TSS_TASK_0:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t0_nivel0_END_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t0_START
         at tss_t.r_eip    , dd __TEXT_t0_lin
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t0_END_lin
         at tss_t.r_es     , dw DS_SEL_0
         at tss_t.r_cs     , dw CS_SEL_0
         at tss_t.r_ss     , dw DS_SEL_0
         at tss_t.r_ds     , dw DS_SEL_0
      iend

SECTION .tss_task_1 progbits

   TSS_TASK_1:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t1_nivel0_END_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t1_START
         at tss_t.r_eip    , dd __TEXT_t1_lin
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t1_END_lin
         at tss_t.r_es     , dw DS_SEL_3
         at tss_t.r_cs     , dw CS_SEL_3
         at tss_t.r_ss     , dw DS_SEL_3
         at tss_t.r_ds     , dw DS_SEL_3
      iend

SECTION .tss_task_2 progbits

  TSS_TASK_2:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t2_nivel0_END_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t2_START
         at tss_t.r_eip    , dd __TEXT_t2_lin
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t2_END_lin
         at tss_t.r_es     , dw DS_SEL_3
         at tss_t.r_cs     , dw CS_SEL_3
         at tss_t.r_ss     , dw DS_SEL_3
         at tss_t.r_ds     , dw DS_SEL_3
      iend

SECTION .tss_task_3 progbits

   TSS_TASK_3:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t3_nivel0_END_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t3_START
         at tss_t.r_eip    , dd __TEXT_t3_lin
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t3_END_lin
         at tss_t.r_es     , dw DS_SEL_3
         at tss_t.r_cs     , dw CS_SEL_3
         at tss_t.r_ss     , dw DS_SEL_3
         at tss_t.r_ds     , dw DS_SEL_3
      iend

SECTION .tss_task_4 progbits

   TSS_TASK_4:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t4_nivel0_END_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t4_START
         at tss_t.r_eip    , dd __TEXT_t4_lin
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t4_END_lin
         at tss_t.r_es     , dw DS_SEL_3
         at tss_t.r_cs     , dw CS_SEL_3
         at tss_t.r_ss     , dw DS_SEL_3
         at tss_t.r_ds     , dw DS_SEL_3
      iend


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .isr32_pit progbits


; ------------------------------------------------------------------------------
; --- struc
; ------------------------------------------------------------------------------
%if (ISR32_IS == A_SCHEDULER)
   GLOBAL isr32_handler_PIT

   isr32_handler_PIT:
      ; Salvo eax para usarlo en el scheduler
      push eax

      .eoi:
         ; Bajar flag de interrupcion -> EOI al PIC1 (0x20)
         mov al, 0x20
         out 0x20, al


      ; ------------------------ need for context switch? ------------------------
      ; Incrementar contadores
      inc byte [count_t1]
      inc byte [count_t2]
      inc byte [count_t3]
      inc byte [count_t4]

      ; Vencieron los contadores?
      cmp byte [count_t1], time_task1
      je reset_count_t1

      cmp byte [count_t2], time_task2
      je reset_count_t2

      cmp byte [count_t3], time_task3
      je reset_count_t3

      cmp byte [count_t4], time_task4
      je reset_count_t4

      mov byte [next_task], task0
      jmp check

      ; Reset contadores
      reset_count_t1:
         mov byte [count_t1], 0x00
         mov byte [next_task], task1
         jmp check

      reset_count_t2:
         mov byte [count_t2], 0x00
         mov byte [next_task], task2
         jmp check

      reset_count_t3:
         mov byte [count_t3], 0x00
         mov byte [next_task], task3
         jmp check

      reset_count_t4:
         mov byte [count_t4], 0x00
         mov byte [next_task], task4
         jmp check

      ; Hay que cambiar de tarea?
      check:
         mov al, [next_task]
         cmp [current_task], al

         pop eax
         je exit


      ; ------------------------ save context ------------------------
      .save_registers:
         mov dword [TSS_TASK_0 + tss_t.r_eax],     eax
         mov dword [TSS_TASK_0 + tss_t.r_ebx],     ebx
         mov dword [TSS_TASK_0 + tss_t.r_ecx],     ecx
         mov dword [TSS_TASK_0 + tss_t.r_edx],     edx
         mov dword [TSS_TASK_0 + tss_t.r_edi],     edi
         mov dword [TSS_TASK_0 + tss_t.r_esi],     esi
         pop eax
         mov dword [TSS_TASK_0 + tss_t.r_eip],     eax   ; eip
         pop eax
         mov word [TSS_TASK_0 + tss_t.r_cs],       ax    ; cs
         mov eax, ds
         mov word [TSS_TASK_0 + tss_t.r_ds],       ax
         mov eax, es
         mov word [TSS_TASK_0 + tss_t.r_es],       ax
         mov eax, cr3
         mov word [TSS_TASK_0 + tss_t.r_cr3],      ax
         pop eax
         mov dword [TSS_TASK_0 + tss_t.r_eflags],  eax   ; eflags
         mov dword [TSS_TASK_0 + tss_t.r_ebp],     ebp

      from_task_with_PL_3:
         cmp byte [TSS_TASK_0 + tss_t.r_cs], CS_SEL_3
         je save_esp_from_stack

      save_esp_from_register:
         mov dword [TSS_TASK_0 + tss_t.r_esp],     esp
         jmp check_t0

      save_esp_from_stack:
         pop eax
         mov dword [TSS_TASK_0 + tss_t.r_esp],     eax   ; esp
         pop eax
         mov word [TSS_TASK_0 + tss_t.r_ss],       ax    ; ss


      ; ------------------------ switch cr3 ------------------------
      check_t0:
         cmp byte [next_task], task0
         je is_t0

      check_t1:
         cmp byte [next_task], task1
         je is_t1

      check_t2:
         cmp byte [next_task], task2
         je is_t2

      check_t3:
         cmp byte [next_task], task3
         je is_t3

      check_t4:
         cmp byte [next_task], task4
         je is_t4

         is_t0:
            mov eax, _PAG_TABLE_t0_START
            jmp change_cr3

         is_t1:
            mov eax, _PAG_TABLE_t1_START
            jmp change_cr3

         is_t2:
            mov eax, _PAG_TABLE_t2_START
            jmp change_cr3

         is_t3:
            mov eax, _PAG_TABLE_t3_START
            jmp change_cr3

         is_t4:
            mov eax, _PAG_TABLE_t4_START

      change_cr3:
         mov cr3, eax

      update_state:
         mov al, [next_task]
         mov [current_task], al


      ; ------------------------ restore context ------------------------
      enable_cr0_ts:
         mov eax, cr0
         or eax, X86_CR0_TS
         mov cr0, eax

      going_to_task_with_same_PL:
         mov ax, [TSS_TASK_0 + tss_t.r_cs]
         mov bx, cs
         cmp ax, bx
         jne different_PL

      same_PL:
         mov dword esp, [TSS_TASK_0 + tss_t.r_esp]    ; esp(3)
         jmp load_registers

      different_PL:
         mov word ax,   [TSS_TASK_0 + tss_t.r_ss]     ; ss(3)
         and dword eax, 0x0000FFFF
         push eax
         mov dword eax, [TSS_TASK_0 + tss_t.r_esp]    ; esp(3)
         push eax

      load_registers:
         mov dword eax, [TSS_TASK_0 + tss_t.r_cr3]
         mov cr3, eax
         mov word ax,   [TSS_TASK_0 + tss_t.r_ds]
         mov ds, ax
         mov word ax,   [TSS_TASK_0 + tss_t.r_es]
         mov es, ax
         mov dword ebp, [TSS_TASK_0 + tss_t.r_ebp]
         mov dword eax, [TSS_TASK_0 + tss_t.r_eflags] ; eflags
         or dword eax, DEFAULT_EFLAGS                 ; enable int
         push eax
         mov word ax,   [TSS_TASK_0 + tss_t.r_cs]     ; cs
         and dword eax, 0x0000FFFF
         push eax
         mov dword eax, [TSS_TASK_0 + tss_t.r_eip]    ; eip
         push eax
         mov dword eax, [TSS_TASK_0 + tss_t.r_eax]
         mov dword ebx, [TSS_TASK_0 + tss_t.r_ebx]
         mov dword ecx, [TSS_TASK_0 + tss_t.r_ecx]
         mov dword edx, [TSS_TASK_0 + tss_t.r_edx]
         mov dword edi, [TSS_TASK_0 + tss_t.r_edi]
         mov dword esi, [TSS_TASK_0 + tss_t.r_esi]

      exit:
         iret

%endif







