; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE16


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "processor-flags.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN A20_Enable_No_Stack
EXTERN gdt_to_gdtr_ROM
EXTERN CS_SEL_0_ROM


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL _init16
GLOBAL A20_Enable_No_Stack_return


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start16 progbits


; ------------------------------------------------------------------------------
; --- _init16
; ------------------------------------------------------------------------------
_init16:
   ; Verificar que el uP no este en fallo
   test  eax, 0x00
   jne   ..@fault_end


   ; Habilito A20
   jmp A20_Enable_No_Stack
   A20_Enable_No_Stack_return:


   ; Cargo GDT
   o32 lgdt [cs:gdt_to_gdtr_ROM]


   ; Establecer el uP en MP
   smsw  ax
   or    ax, X86_CR0_PE
   lmsw  ax


   o32 jmp dword CS_SEL_0_ROM:_init32




   ; Error
   ..@fault_end:
      hlt
      jmp ..@fault_end








; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN DS_SEL_0_ROM
EXTERN __STACK_END
EXTERN __STACK_SIZE
EXTERN td3_memcopy
EXTERN ___systables_size
EXTERN ___systables_lma
EXTERN ___systables_vma
EXTERN ___isq_size
EXTERN ___isq_lma
EXTERN ___isq_vma
EXTERN ___kernel_size
EXTERN ___kernel_vma_st
EXTERN ___kernel_lma_st
EXTERN ___TEXT_task_1_size
EXTERN ___TEXT_task_1_lma_st
EXTERN ___TEXT_task_1_phy_st
EXTERN ___DATA_size
EXTERN ___DATA_lma_st
EXTERN ___DATA_vma_st
EXTERN gdt_to_gdtr
EXTERN idt_to_idtr
EXTERN init_irq
EXTERN init_pagetables
EXTERN init_pagetables_c
EXTERN CS_SEL_0
EXTERN __PAG_TABLE_START
EXTERN kernel


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL init_irq_ret
GLOBAL init_pagetables_ret


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start32 progbits


; ------------------------------------------------------------------------------
; --- _init32
; ------------------------------------------------------------------------------
_init32:

   .init_segments:
      ; Inicializar la pila en segmento DS  
      mov ax, DS_SEL_0_ROM
      mov ss, ax
      mov esp, __STACK_END

      ; Limpiar el stack
      xor ebx, ebx
      mov ecx, __STACK_SIZE

      .stack_init:
         push ebx
         loop .stack_init
      mov esp, __STACK_END

      ; Inicializar la selectores datos
      mov ds, ax
      mov es, ax
      mov gs, ax
      mov fs, ax


   .copy_to_ram:
      ; Desempaquetar la ROM
      ; systables
      push ebp
      mov ebp, esp
      push ___systables_size     ;  arg3  -> cantidad
      push ___systables_lma      ;  arg2  -> origen
      push ___systables_vma      ;  arg1  -> destino
      call td3_memcopy
      leave

      ; isr
      push ebp
      mov ebp, esp
      push ___isq_size           ;  arg3  -> cantidad
      push ___isq_lma            ;  arg2  -> origen
      push ___isq_vma            ;  arg1  -> destino
      call td3_memcopy
      leave

      ; kernel
      push ebp
      mov ebp, esp
      push ___kernel_size        ;  arg3  -> cantidad
      push ___kernel_lma_st      ;  arg2  -> origen
      push ___kernel_vma_st      ;  arg1  -> destino
      call td3_memcopy
      leave

      ; TEXT_task_1
      push ebp
      mov ebp, esp
      push ___TEXT_task_1_size        ;  arg3  -> cantidad
      push ___TEXT_task_1_lma_st      ;  arg2  -> origen
      push ___TEXT_task_1_phy_st      ;  arg1  -> destino
      call td3_memcopy
      leave

      ; DATA
      push ebp
      mov ebp, esp
      push ___DATA_size        ;  arg3  -> cantidad
      push ___DATA_lma_st      ;  arg2  -> origen
      push ___DATA_vma_st      ;  arg1  -> destino
      call td3_memcopy
      leave


   .load_systables:
      ; Cargar GDT ya copiada en RAM
      o32 lgdt [gdt_to_gdtr]
      ; Cargar IDT ya copiada en RAM
      lidt [idt_to_idtr]


   ; Inicializar irq
   jmp init_irq
   init_irq_ret:


   ; Cargar tablas paginacion con rutina en assembler
;   jmp init_pagetables
   init_pagetables_ret:

   ; Cargar tablas paginacion con rutina en C
   call init_pagetables_c

   .init_paging:
      ; cargo cr3
      mov eax, __PAG_TABLE_START
;      or eax, X86_CR3_PCD
;      or eax, X86_CR3_PWT
      mov cr3, eax


   jmp kernel







