; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL kybrd_data
GLOBAL tabla_digitos_count
GLOBAL timer_count
GLOBAL last_count
GLOBAL page_addr_phy


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .datos progbits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss
; ------------------------------------------------------------------------------

; Variables del keyboard
   kybrd_data:
      db 0x00          ; Reservo 1 byte

   tabla_digitos_count:
      dd 0x00


; Variables de timer, contadores hasta 10 (500ms)
   timer_count:
      db 0x00          ; Reservo 1 byte

   last_count:
      db 0x00          ; Reservo 1 byte


; Variables de ISR14: PAGE FAULT
   page_addr_phy:
      dd 0x10000000    ; Reservo 1 dword







