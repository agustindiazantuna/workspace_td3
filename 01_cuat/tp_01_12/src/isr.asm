; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define PS2_DPORT             0x60
%define PS2_SREG              0x64
%define PS2_ACK               0x20
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN kybrd_data
EXTERN timer_count

; ISR14
EXTERN ptree_entry
EXTERN __PAG_TABLE_START
EXTERN page_addr_phy


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL isr0_handler_DE
GLOBAL isr2_handler_NMI
GLOBAL isr3_handler_BP
GLOBAL isr4_handler_OF
GLOBAL isr5_handler_BR
GLOBAL isr6_handler_UD
GLOBAL isr7_handler_NM
GLOBAL isr8_handler_DF
GLOBAL isr10_handler_TS
GLOBAL isr11_handler_NP
GLOBAL isr12_handler_SS
GLOBAL isr13_handler_GP
GLOBAL isr14_handler_PF
GLOBAL isr16_handler_MF
GLOBAL isr17_handler_AC
GLOBAL isr18_handler_MC
GLOBAL isr19_handler_XF
GLOBAL isr32_handler_PIT
GLOBAL isr33_handler_KEYBOARD


; ------------------------------------------------------------------------------
; progbits : datos definidos
; nobits : datos no inicializados
SECTION .isr progbits


; ------------------------------------------------------------------------------
; --- default_isr
; ------------------------------------------------------------------------------
; Source:
;           https://wiki.osdev.org/Exceptions
;           https://wiki.osdev.org/Interrupts
;           

default_isr:
   nop
   BOCHS_STOP
   nop
   hlt


isr0_handler_DE:
   mov edx, 0
   jmp default_isr


isr2_handler_NMI:
   mov edx, 2
   jmp default_isr


isr3_handler_BP:
   mov edx, 3
   jmp default_isr


isr4_handler_OF:
   mov edx, 4
   jmp default_isr


isr5_handler_BR:
   mov edx, 5
   jmp default_isr


isr6_handler_UD:
   mov edx, 6
   jmp default_isr


isr7_handler_NM:
   mov edx, 7
   jmp default_isr


isr8_handler_DF:
   mov edx, 8
   jmp default_isr


isr10_handler_TS:
   mov edx, 10
   jmp default_isr


isr11_handler_NP:
   mov edx, 11
   jmp default_isr


isr12_handler_SS:
   mov edx, 12
   jmp default_isr


isr13_handler_GP:
   mov edx, 13
   jmp default_isr


isr14_handler_PF:
   pushad
   mov edx, 14

   ; ---------------------------------------------------------
   ; --- Stack frame de Page Fault:
   ; ---------------------------------------------------------
   ; ERROR_CODE <- esp actual
   ; eip
   ; cs
   ; eflags
   ; <- esp anterior
   ;
   ; ---------------------------------------------------------
   ; --- ERROR_CODE
   ; ---------------------------------------------------------
   ; 4   I/D   opcode fetch ¿?
   ; 3   RSVD  bits reservados ¿?
   ; 2   U/S   Privilegios incorrectos
   ; 1   R/W   User intenta escribir en Read Only
   ; 0   P     Si el bit P (presente) del page_attr esta en 0
   ; 
   ; ---------------------------------------------------------
   ; --- PAGINA NO CREADA
   ; ---------------------------------------------------------
   ; page_attr = 0x00
   ;  7   PS
   ;  5   NO ACCEDIDO
   ;  4   PCD
   ;  3   PWT
   ;  2   PRIVILEGIO KERNEL
   ;  1   READ
   ;  0   NO PRESENTE
   ;
   ; ---------------------------------------------------------

   ; Leer ERROR_CODE del stack (esta 8 posiciones mas abajo)
;   mov eax, [esp + 32]

   ; Verifico ERROR_CODE
;  si corresponde

   ; Creo pagina pedida
   .createpage:
      push ebp
      mov ebp, esp

      push 3                     ; page_attr
      push 3                     ; table_attr
      mov edx, cr2
      push edx                   ; addr_lin
      push dword [page_addr_phy] ; addr_phy
      push __PAG_TABLE_START     ; ptree_entry
      call ptree_entry
      leave

   ; Incremento la pagina fisica entregada
   add dword [page_addr_phy], 0x00001000

   ; Reviso valor de retorno
   cmp eax, 0
   jne .returnerror

   ; Vuelvo a la funcion
   popad
   add esp, 4     ; extraigo ERROR_CODE
   iret

   .returnerror:
      jmp default_isr


isr16_handler_MF:
   mov edx, 16
   jmp default_isr


isr17_handler_AC:
   mov edx, 17
   jmp default_isr


isr18_handler_MC:
   mov edx, 18
   jmp default_isr


isr19_handler_XF:
   mov edx, 19
   jmp default_isr


; Interrupcion de PIT
isr32_handler_PIT:
   pushad

   .count50ms:
      inc byte [timer_count]

   .eoi:
      ; Bajar flag de interrupcion -> EOI al PIC1 (0x20)
      mov al, 0x20
      out 0x20, al
   
   popad

   iret


; Interrupcion de teclado
isr33_handler_KEYBOARD:
   push eax

   ; Leer DATA_PORT
   in al, PS2_DPORT

   ; Copio valor a memoria
   mov byte [kybrd_data], al

   ; Bajar flag de interrupcion -> EOI al PIC1 (0x20)
   mov al, 0x20
   out 0x20, al

   pop eax
   
   iret







