; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "processor-flags.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define NULL_KEY              0xEE
%define BOCHS_STOP            xchg bx,bx


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN __PAG_TABLE_START
EXTERN kybrd_data
EXTERN alfabeto_h
EXTERN read_table_h
EXTERN timer_count
EXTERN last_count
EXTERN tabla_digitos_count
EXTERN add_tabla_digitos


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL kernel


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .kernel progbits


; ------------------------------------------------------------------------------
; --- kernel
; ------------------------------------------------------------------------------
kernel:
   .enable_paging:
      mov eax, cr0
      or eax, X86_CR0_PG
      mov cr0, eax


;      BOCHS_STOP
;      mov dword eax, 0x5000

      ; leer
;      mov dword ebx, [eax]
      ; escribir
;      mov dword [eax], 0xABCDABCD


   mov byte [kybrd_data], NULL_KEY


   while1:

      up_halt:
         hlt

         ; Si pasaron 500ms
         cmp byte [timer_count], 0x0A
         je suma_tabla_digitos

         ; Si la interrupcion fue de teclado
         mov al, [last_count]
         cmp byte [timer_count], al
         je keyboard_routine

         ; Sino fue de timer pero no pasaron 500ms
         mov al, [timer_count]
         mov byte [last_count], al

         jmp up_halt 


      suma_tabla_digitos:
         call add_tabla_digitos

         ; Limpio cuenta
         and byte [timer_count], 0x00

         jmp up_halt


      keyboard_routine:
         ; Tecla leida
         mov eax, [kybrd_data]

         ; Si es un codigo release vuelve al up_halt
         bt eax, 0x07           ; Guarda el bit 7 de eax en CF flag
         jc up_halt             ; Si CF = 1 => codigo release

         ; Funcion filtrado tecla + guardado
         push eax
         call alfabeto_h
         pop eax

         ; Limpio buffer
         mov byte [kybrd_data], NULL_KEY

         ; Compruebo escritura
         push eax
         call read_table_h
         pop eax

         jmp while1




   .guard:
      hlt
      jmp .guard







