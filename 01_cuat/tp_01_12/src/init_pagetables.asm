; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define k_table_attr_w        0x0003
%define k_page_attr_w         0x0003


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN ptree_entry
EXTERN __PAG_TABLE_START
EXTERN ___isq_vma
EXTERN ___systables_vma
EXTERN ___kernel_vma_st
EXTERN __TABLA_DIGITOS_START
EXTERN ___DATA_vma_st
EXTERN __STACK_START
EXTERN __STACK_PAGE1
EXTERN __STACK_PAGE2
EXTERN init_pagetables_ret


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL init_pagetables


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start32 progbits


; ------------------------------------------------------------------------------
; --- init_pagetables
; ------------------------------------------------------------------------------
init_pagetables:


; __UTILS32__ void ptree_entry(dword ptree_base, dword addr_phy,
;                              dword addr_lin, word table_attr, 
;                              word page_attr)
;
;
; ------------CODIGO---------------
; --------------------------------- page_attr   0x0003
;     8  G        0 : Global
;
;     7  PAT      0 : 
;     6  D        0 : Dirty, no modificada
;     5  A        0 : Accedida
;     4  PCD      0 : Cache disable
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel) - 1 : Usuario
;     1  R/W      1 : 0 R - 1 W
;     0  P        1 : Presente
; --------------------------------- table_attr  0x0003
;     7  PS       0 : 4KB
;     6  IGN      0 : Ignorado
;     5  A        0 : Accedida
;     4  PCD      0
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel)
;     1  R/W      1 : 0 R - 1 W
;     0  P        1
;
;
; ------------DATA---------------
; --------------------------------- page_attr   0x0103
;     8  G        0 : Global
;
;     7  PAT      0
;     6  D        0 : Dirty, no modificada
;     5  A        0 : Accedida
;     4  PCD      0
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel)
;     1  R/W      1 : 0 R - 1 W
;     0  P        1
; --------------------------------- table_attr  0x0003
;     7  PS       0 : 4KB
;     6  IGN      0 : Ignorado
;     5  A        0 : Accedida
;     4  PCD      0
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel)
;     1  R/W      1 : 0 R - 1 W
;     0  P        1

   .load_isr:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push ___isq_vma            ;  arg3  -> addr_lin
      push ___isq_vma            ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_systables:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push ___systables_vma      ;  arg3  -> addr_lin
      push ___systables_vma      ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_pagetables:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __PAG_TABLE_START     ;  arg3  -> addr_lin
      push __PAG_TABLE_START     ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_kernel:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push ___kernel_vma_st      ;  arg3  -> addr_lin
      push ___kernel_vma_st      ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_tabla_digitos:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __TABLA_DIGITOS_START ;  arg3  -> addr_lin
      push __TABLA_DIGITOS_START ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_data:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push ___DATA_vma_st        ;  arg3  -> addr_lin
      push ___DATA_vma_st        ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_stack:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __STACK_PAGE1         ;  arg3  -> addr_lin
      push __STACK_PAGE1         ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __STACK_PAGE2         ;  arg3  -> addr_lin
      push __STACK_PAGE2         ;  arg2  -> addr_phy
      push __PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave


   jmp init_pagetables_ret







