/** 
   \file    task1.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 1

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */
// Tabla digitos
extern qword tabla_digitos[1000];
extern dword tabla_digitos_count;


// Variables local de la tarea 1
dword suma_tabla_digitos __TASK_1_BSS__ = 0;


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__TASK_1_TEXT__ void add_tabla_digitos(void)
{
   qword i = 0;
   dword *pointer = 0;
   dword lectura = 0;

   suma_tabla_digitos = 0;

   for(i = 0; i < tabla_digitos_count; i++)
      suma_tabla_digitos += tabla_digitos[i];

   // Trato de leer y escribir la posicion de memoria indicada por la suma
   if(suma_tabla_digitos < 0x20000000 && suma_tabla_digitos != 0) // 512MB
   {
      pointer = (dword *) suma_tabla_digitos;
      lectura = *pointer;
      *pointer = lectura;
   }
}







