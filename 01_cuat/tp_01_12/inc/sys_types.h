/** 
   \file  sys_types.h   
   \version 01.00
   \brief Biblioteca de declaraciones y definiciones relacionadas con el SO.

   \author Christian Nigri <cnigri@frba.utn.edu.ar>
   \date    01/02/2012
*/
#ifndef _SYS_TYPES_H_
   #define _SYS_TYPES_H_

//#define SISTEMA IA32
/** \def ON */
#define ON     1              ///<Encendido
/** \def OFF */
#define OFF    0              ///<Apagado
/** \def EXITO */
#define EXITO  1              ///<Valor retornado en caso de una ejecucion exitosa de lo solicitado
/** \def ERROR_DEFECTO */
#define ERROR_DEFECTO      -1 ///<Error debido a que no se puede resolver/ejecutar lo solicitado 
/** \def ERROR_DESCONOCIDO */
#define ERROR_DESCONOCIDO  -2 ///<Error del cual no se conoce su origen

/** \def BYTE */
typedef unsigned char byte;   ///<Tipo byte
/** \def WORD */
typedef unsigned short word;  ///<Tipo word
/** \def DWORD */
typedef unsigned long dword;  ///<Tipo dword
/** \def QWORD */
typedef unsigned long long qword;   ///<Tipo qword
/** \def TICKS_T */
typedef unsigned long long int ticks_t;   ///<Tipo para timer tick


/* attribute */
#define __DATA__           __attribute__(( section(".datos")))
#define __FUNCTIONS__      __attribute__(( section(".functions")))
#define __TABLA_DIGITOS__  __attribute__(( section(".tabla_digitos")))
#define __UTILS32__        __attribute__(( section(".utils32")))
#define __UTILS32_RAM__    __attribute__(( section(".utils32_RAM")))
#define __TASK_1_TEXT__    __attribute__(( section(".TEXT_task_1")))
#define __TASK_1_BSS__     __attribute__(( section(".BSS_task_1")))


/* breakpoint */
#define BOCHS_STOP         asm("xchg %%bx,%%bx"::);


#endif  /*_SYS_TYPES_H_*/