;-------------------------------------------------------------------------------
;|  Título:         Memory Copy                                                |
;|  Versión:        1.0                     Fecha:  24/04/2018                 |
;|  Autor:          Diaz Antuna, Agustin    Modelo: IA-32 (16/32bits)          |
;|  ------------------------------------------------------------------------   |
;|  Descripción:                                                               |
;|      Realiza una copia desde una region de memoria a otra                   |
;|  ------------------------------------------------------------------------   |
;|  Recibe:                                                                    |
;|      destino:     Direccion de destino                                      |
;|      origen:      Direccion de origen                                       |
;|      num_bytes:   Cantidad de bytes a copiar                                |
;|                                                                             |
;|  Retorna:                                                                   |
;|      Nada                                                                   |
;|  ------------------------------------------------------------------------   |
;|  Revisiones:                                                                |
;|      1.0 | 24/04/2018 | Diaz Antuna, Agustin | Original                     |
;-------------------------------------------------------------------------------

; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32

; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL td3_memcopy

; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .utils32 progbits

; ------------------------------------------------------------------------------
; --- td3_memcopy
; ------------------------------------------------------------------------------

;  Funcion pedida:
;  void *td3_memcopy(void *destino, const void *origen, unsigned int num_bytes);

td3_memcopy:

   ; Pusheo registros implicados
   push ebp       ;  Pusheo Base Pointer para guardar su contenido actual
   mov ebp, esp   ;  En el BP guardo el SP que esta al nivel de la pila

   push edi
   push esi
   push ecx
   push eax

   ; Obtengo los argumentos del stack de 16bits
   ;  ebp      <---  ebp
   ;  ip       <---  ebp+4
   ;  arg1     <---  ebp+8
   ;  arg2     <---  ebp+12
   ;  arg3     <---  ebp+16

   mov edi, [ebp+8]        ;  1er arg -> destino
   mov esi, [ebp+12]       ;  2do arg -> origen
   mov ecx, [ebp+16]       ;  3er arg -> cantidad

   ; Realizo funcion
   lazo:
      mov al,[esi]
      mov [edi],al
      add edi,1
      add esi,1
   loop lazo

   ; Popeo registros implicados devuelvo lo pedido
   pop eax
   pop ecx
   pop esi
   pop edi
   pop ebp

;   mov eax, [esp+2]        ;  Devuelvo el puntero destino

   ret                     ;  Popeo IP







