/** 
   \file    task1.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 1

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */
// Tabla digitos
extern qword tabla_digitos[1000];
extern dword tabla_digitos_count;


// Variables local de la tarea 1
dword suma_tabla_digitos_t1 __T1_BSS__ = 0;


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__T1_TEXT__ void task01(void)
{

   #if (TASK01_IS == DUMMY)

      while(1)
      {
         BOCHS_STOP
         asm("mov %0,%%eax"::"r"(0xFFFF0001));
         asm("hlt");
      }

   #elif (TASK01_IS == ON)

      qword i = 0;
   //   dword *pointer = 0;
   //   dword lectura = 0;


      while(1)
      {
   /*
         if(suma_tabla_digitos_t1 == 0x5000)
         {
   //         BOCHS_STOP
            asm("mov %0,%%eax"::"r"(0xFFFF0001));
            asm("mov %0,%%eax"::"r"(suma_tabla_digitos_t1));
         }
   */

         suma_tabla_digitos_t1 = 0;

         for(i = 0; i < tabla_digitos_count; i++)
            suma_tabla_digitos_t1 += tabla_digitos[i];


         asm("hlt");

   /*
         // Trato de leer y escribir la posicion de memoria indicada por la suma
         if(suma_tabla_digitos_t1 < 0x20000000 && suma_tabla_digitos_t1 != 0) // 512MB
         {
            pointer = (dword *) suma_tabla_digitos_t1;
            lectura = *pointer;
            *pointer = lectura;
         }
   */
      }

   #endif

}







