; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


; ------------------------------------------------------------------------------
; --- include
; ------------------------------------------------------------------------------
%include "tss-table.h"
%include "config.h"


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define BOCHS_STOP            xchg bx,bx
%define task0   0
%define task1   1
%define task2   2


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN CS_SEL_0
EXTERN DS_SEL_0
EXTERN current_task
EXTERN next_task

EXTERN _PAG_TABLE_t0_START
EXTERN _STACK_t0_nivel0_START_lin
EXTERN __TEXT_t0_lin
EXTERN _STACK_t0_END_lin
EXTERN _PAG_TABLE_t2_START
EXTERN _STACK_t2_nivel0_START_lin
EXTERN __TEXT_t2_lin
EXTERN _STACK_t2_END_lin
EXTERN _PAG_TABLE_t1_START
EXTERN _STACK_t1_nivel0_START_lin
EXTERN __TEXT_t1_lin
EXTERN _STACK_t1_END_lin

EXTERN count_t1
EXTERN count_t2


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- tss
; ------------------------------------------------------------------------------
SECTION .tss_task_0 progbits

  TSS_TASK_0:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t0_nivel0_START_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t0_START
         at tss_t.r_eip    , dd __TEXT_t0_lin + 0x00
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t0_END_lin - 0x04
         at tss_t.r_es     , dw DS_SEL_0
         at tss_t.r_cs     , dw CS_SEL_0
         at tss_t.r_ss     , dw DS_SEL_0
         at tss_t.r_ds     , dw DS_SEL_0
      iend

SECTION .tss_task_2 progbits

  TSS_TASK_2:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t2_nivel0_START_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t2_START
         at tss_t.r_eip    , dd __TEXT_t2_lin + 0x00
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t2_END_lin - 0x04
         at tss_t.r_es     , dw DS_SEL_0
         at tss_t.r_cs     , dw CS_SEL_0
         at tss_t.r_ss     , dw DS_SEL_0
         at tss_t.r_ds     , dw DS_SEL_0
      iend

SECTION .tss_task_1 progbits

   TSS_TASK_1:
      istruc   tss_t
         at tss_t.r_esp0   , dd _STACK_t1_nivel0_START_lin
         at tss_t.r_ss0    , dw DS_SEL_0
         at tss_t.r_cr3    , dd _PAG_TABLE_t1_START
         at tss_t.r_eip    , dd __TEXT_t1_lin + 0x00
         at tss_t.r_eflags , dd 0x202
         at tss_t.r_esp    , dd _STACK_t1_END_lin - 0x04
         at tss_t.r_es     , dw DS_SEL_0
         at tss_t.r_cs     , dw CS_SEL_0
         at tss_t.r_ss     , dw DS_SEL_0
         at tss_t.r_ds     , dw DS_SEL_0
      iend


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .isr32_pit progbits


; ------------------------------------------------------------------------------
; --- struc
; ------------------------------------------------------------------------------
%if (ISR32_IS == A_SCHEDULER)
   GLOBAL isr32_handler_PIT

   isr32_handler_PIT:

      ; Salvo eax
      push eax

      .eoi:
         ; Bajar flag de interrupcion -> EOI al PIC1 (0x20)
         mov al, 0x20
         out 0x20, al


      ; ------------------------ need for switch context? ------------------------
      ; Incrementar contadores
      inc byte [count_t1]
      inc byte [count_t2]

      ; Vencieron los contadores?
      cmp byte [count_t1], 100
      je reset_count_t1

      cmp byte [count_t2], 200
      je reset_count_t2

      mov byte [next_task], task0
      jmp check

      ; Reset contadores
      reset_count_t1:
         mov byte [count_t1], 0x00
         mov byte [next_task], task1
         jmp check

      reset_count_t2:
         mov byte [count_t2], 0x00
         mov byte [next_task], task2
         jmp check

      ; Si hay que cambiar de tarea
      check:
         mov al, [next_task]
         cmp [current_task], al

         pop eax
         je exit


      ; ------------------------ save context ------------------------
      .save_registers:
         mov dword [TSS_TASK_0 + tss_t.r_eax], eax
         mov dword [TSS_TASK_0 + tss_t.r_ebx], ebx
         mov dword [TSS_TASK_0 + tss_t.r_ecx], ecx
         mov dword [TSS_TASK_0 + tss_t.r_edx], edx
         mov dword [TSS_TASK_0 + tss_t.r_edi], edi
         mov dword [TSS_TASK_0 + tss_t.r_esi], esi
         pop eax
         mov dword [TSS_TASK_0 + tss_t.r_eip], eax  ; eip
         pop eax
         mov word [TSS_TASK_0 + tss_t.r_cs], ax
         mov eax, ds
         mov word [TSS_TASK_0 + tss_t.r_ds], ax
         mov eax, ss
         mov word [TSS_TASK_0 + tss_t.r_ss], ax
         mov eax, es
         mov word [TSS_TASK_0 + tss_t.r_es], ax
         mov eax, cr3
         mov word [TSS_TASK_0 + tss_t.r_cr3], ax
         pop eax
         mov dword [TSS_TASK_0 + tss_t.r_eflags], eax
         mov dword [TSS_TASK_0 + tss_t.r_ebp], ebp
         mov dword [TSS_TASK_0 + tss_t.r_esp], esp


      ; ------------------------ switch cr3 ------------------------
      check_t0:
         cmp byte [next_task], task0
         je is_t0

      check_t1:
         cmp byte [next_task], task1
         je is_t1

      check_t2:
         cmp byte [next_task], task2
         je is_t2

         is_t0:
            mov eax, _PAG_TABLE_t0_START
            jmp change_cr3

         is_t1:
            mov eax, _PAG_TABLE_t1_START
            jmp change_cr3

         is_t2:
            mov eax, _PAG_TABLE_t2_START

      change_cr3:
         mov cr3, eax

      update_state:
         mov eax, [next_task]
         mov [current_task], eax


      ; ------------------------ restore context ------------------------
      .load_registers:
         mov dword ebp, [TSS_TASK_0 + tss_t.r_ebp]
         mov dword esp, [TSS_TASK_0 + tss_t.r_esp]
         mov dword eax, [TSS_TASK_0 + tss_t.r_eflags]
         or dword eax, 0x0202                      ; enable int
         push eax
         mov word ax, [TSS_TASK_0 + tss_t.r_cs]
         push eax
         mov word ax, [TSS_TASK_0 + tss_t.r_ds]
         mov ds, eax
         mov word ax, [TSS_TASK_0 + tss_t.r_ss]
         mov ss, eax
         mov word ax, [TSS_TASK_0 + tss_t.r_es]
         mov es, eax
         mov dword eax, [TSS_TASK_0 + tss_t.r_cr3]
         mov cr3, eax
         mov dword eax, [TSS_TASK_0 + tss_t.r_eip]  ; eip
         push eax
         mov dword eax, [TSS_TASK_0 + tss_t.r_eax]
         mov dword ebx, [TSS_TASK_0 + tss_t.r_ebx]
         mov dword ecx, [TSS_TASK_0 + tss_t.r_ecx]
         mov dword edx, [TSS_TASK_0 + tss_t.r_edx]
         mov dword edi, [TSS_TASK_0 + tss_t.r_edi]
         mov dword esi, [TSS_TASK_0 + tss_t.r_esi]


      exit:
         iret

%endif







