; -------------------------------------------------------------------------------
; --- GDT
; -------------------------------------------------------------------------------

struc   gdtd_t                   ;Definicion de la estructura denominada gdtd_t, la cual contiene los siguientes campos
       .lim_00_15:      resw 1   ;Limite del segmento bits 00-15.
       .base00_15:      resw 1   ;Direccion base del segmento bits 00-15.
       .base16_23:      resb 1   ;Direccion base del segmento bits 16-23.
       .prop:           resb 1   ;Propiedades.
       .lim_prop:       resb 1   ;Limite del segmento 16-19 y propiedades.
       .base24_31:      resb 1   ;Direccion base del segmento bits 24-31. 
endstruc

GDT:
NULL_SEL    equ $-GDT
   dq 0x0
CS_SEL_0    equ $-GDT
   istruc gdtd_t
      at gdtd_t.lim_00_15, dw 0xffff
      at gdtd_t.base00_15, dw 0x0000
      at gdtd_t.base16_23, db 0x00
      at gdtd_t.prop,      db 10010011b
      at gdtd_t.lim_prop,  db 11001111b
      at gdtd_t.base24_31, db 0
   iend
DS_SEL_0    equ $-GDT
   istruc gdtd_t
      at gdtd_t.lim_00_15, dw 0xffff
      at gdtd_t.base00_15, dw 0x0000
      at gdtd_t.base16_23, db 0x00
      at gdtd_t.prop,      db 10010001b
      at gdtd_t.lim_prop,  db 11001111b
      at gdtd_t.base24_31, db 0
   iend
GDT_LENGTH  equ $-GDT





%if 0
GDT:
NULL_SEL    equ $-GDT
   dq 0x0
CS_SEL_0    equ $-GDT
   dw 0xffff 
   dw 0x0000
   db 0x00
   db 0x92
   db 0xcf
   db 0
GDT_LENGTH  equ $-GDT
%endif






gdtr:
   dw GDT_LENGTH-1
   dd GDT
