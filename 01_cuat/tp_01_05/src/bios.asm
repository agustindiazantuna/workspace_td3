; -------------------------------------------------------------------------------
; --- Includes
; -------------------------------------------------------------------------------
%include "processor-flags.h"








; -------------------------------------------------------------------------------
; --- Directivas
; -------------------------------------------------------------------------------

USE16       ; Le indicamos al codigo que trabaje en modo 16bits
            ; Los registros ocupan 2 bytes en stack








; -------------------------------------------------------------------------------
; --- Defines
; -------------------------------------------------------------------------------

%define ROM_SIZE        (64*1024)
%define ROM_START       0xf0000
%define RESET_VECTOR    0xffff0

%define DESTINO_1_es    0x0000         ; es
%define DESTINO_1_di    0x0000         ; di

%define ORIGEN_2        0x00000000
%define DESTINO_2_es    0x0000         ; es
%define DESTINO_2_di    0x00300000     ; di








   ; -------------------------------------------------------
   ; punto i
   ; -------------------------------------------------------
   ; td3_memcopy
   mov di, DESTINO_1_di       ;  1er arg - DESTINO
   mov si, ROM_START          ;  2do arg - ORIGEN
   mov cx, ROM_SIZE-1         ;  3er arg - CANTIDAD

   lazo:
      mov ax,[cs:si]
      mov [di],ax
      add di,2
      add si,2
   loop lazo

   ; Salto a la primer copia
   jmp 0x0000:afterjmp_1
   afterjmp_1:

   ; -------------------------------------------------------
   ; Para moverme por los 4GB cargo GDT
   ; -------------------------------------------------------
   o32 lgdt  [cs:gdtr]
   ;->Establecer el up en MP<-
   smsw  ax
   or    ax, X86_CR0_PE
   lmsw  ax

   mov ax, CS_SEL_0
   mov ds, ax

   ;->Quitar el up de MP<-
   mov  eax, cr0
   and  eax, 0xfffffffe
   mov  cr0, eax

   ; -------------------------------------------------------
   ; punto ii
   ; -------------------------------------------------------
   ; td3_memcopy
   mov edi, DESTINO_2_di      ;  1er arg - DESTINO
   mov esi, ORIGEN_2          ;  2do arg - ORIGEN
   mov ecx, ROM_SIZE-1        ;  3er arg - CANTIDAD

   lazo2:
      mov ax,[cs:si]
      mov [edi],ax
      add edi,2
      add si,2
   loop lazo2
%if 0
   mov eax, DESTINO_2_di
   add eax, afterjmp_2

   ; Salto a la segunda copia
   jmp eax
   afterjmp_2:
%endif
   ; Defino registros del stack --> ss:sp --> 000F:FFFF
   ; Establecer la pila en 0x1FFFB000 
   mov eax, 0x1FFF;0x000F;0xF000
   mov ss, eax

   mov eax, 0xB000;0xFFFF
   mov esp, eax

   hlt








; -------------------------------------------------------------------------------
; --- GDT
; -------------------------------------------------------------------------------

struc   gdtd_t                   ;Definicion de la estructura denominada gdtd_t, la cual contiene los siguientes campos
       .lim_00_15:      resw 1   ;Limite del segmento bits 00-15.
       .base00_15:      resw 1   ;Direccion base del segmento bits 00-15.
       .base16_23:      resb 1   ;Direccion base del segmento bits 16-23.
       .prop:           resb 1   ;Propiedades.
       .lim_prop:       resb 1   ;Limite del segmento 16-19 y propiedades.
       .base24_31:      resb 1   ;Direccion base del segmento bits 24-31. 
endstruc

GDT:
NULL_SEL    equ $-GDT
   dq 0x0
CS_SEL_0    equ $-GDT
   istruc gdtd_t
      at gdtd_t.lim_00_15, dw 0xffff
      at gdtd_t.base00_15, dw 0x0000
      at gdtd_t.base16_23, db 0x00
      at gdtd_t.prop,      db 10010011b
      at gdtd_t.lim_prop,  db 11001111b
      at gdtd_t.base24_31, db 0
   iend
DS_SEL_0    equ $-GDT
   istruc gdtd_t
      at gdtd_t.lim_00_15, dw 0xffff
      at gdtd_t.base00_15, dw 0x0000
      at gdtd_t.base16_23, db 0x00
      at gdtd_t.prop,      db 10010001b
      at gdtd_t.lim_prop,  db 11001111b
      at gdtd_t.base24_31, db 0
   iend
GDT_LENGTH  equ $-GDT

%if 0
GDT:
NULL_SEL    equ $-GDT
   dq 0x0
CS_SEL_0    equ $-GDT
   dw 0xffff 
   dw 0x0000
   db 0x00
   db 0x92
   db 0xcf
   db 0
GDT_LENGTH  equ $-GDT
%endif

gdtr:
   dw GDT_LENGTH-1
   dd GDT








; -------------------------------------------------------------------------------
; --- Fault_end
; -------------------------------------------------------------------------------

..@fault_end:
   hlt
   jmp ..@fault_end















; -------------------------------------------------------------------------------
; --- El codigo arranca en 0xFFFF0
; -------------------------------------------------------------------------------

   CODE_LENGTH equ ($-start16)

   ; ocupa espacio con nop para posicionar el codigo en 0xFFFF0
   times (RESET_VECTOR - ROM_START - CODE_LENGTH) nop 







