USE32


; ------------esto va a main.asm

SECTION .kernel32_init progbits
EXTERN DS_SEL_0
EXTERN CS_SEL_0
EXTERN __STACK_END_32
EXTERN __STACK_SIZE_32

GLOBAL kernel32_init

kernel32_init;
   ; Inicializo los selectores
   mov ax, DS_SEL_0
   mov ss, ax
   mov esp, __STACK_END_32
   mov ds, ax
   ; Inicializo la pila
   mov ecx, __STACK_SIZE_32
   xor eax, eax
   .stack_init:
      push eax
      loop .stack_init
   mov esp, __STACK_END_32



