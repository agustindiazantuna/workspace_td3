%include "processor-flags.h"

%define ROM_SIZE     (64*1024)
%define ROM_START    0xf0000
%define RESET_VECTOR 0xffff0

ORG ROM_START
USE16
start16:
   test eax, 0x0             ;Verificar que el uP no este en fallo
   jne ..@fault_end

   xor eax, eax
   mov cr3, eax             ;Invalidar TLB
  
   jmp A20_Enable_No_Stack
   A20_Enable_No_Stack_return:

   ;--> TEST 00 INICIO<--   
   mov cx, 65535
   xor di, di   
   .cp0:
   mov byte [di], 0xa5
   inc di
   loop .cp0

   mov byte [di], 0x5a
   inc di
   mov byte [di], 0x12
   inc di
   mov byte [di], 0x34
   inc di
   mov byte [di], 0x56
   inc di
   mov byte [di], 0x78
   ;--> TEST 00 FIN<--
%if 0
;--> TEST 01 INICIO<--
   mov cx, 65535
   xor di, di
   .cp1:
   mov byte [di], 0xff
   inc di
   loop .cp1

   mov byte [edi], 0x00
   inc edi
   mov byte [edi], 0x00
   inc edi
   mov byte [edi], 0x00
   inc edi
   mov byte [edi], 0x00
   inc edi
   mov byte [edi], 0x00
   ;--> TEST 01 FIN<--
%endif
   ;--> TEST 02 INICIO<--
   mov cx, 65535
   xor di, di   
   .cp2:
   mov byte [di], 0xff
   inc di
   loop .cp2
   
   o32 lgdt  [cs:gdtr]
   ;->Establecer el up en MP<-
   smsw  ax
   or    ax, X86_CR0_PE
   lmsw  ax

   mov ax, DS_SEL
   mov ds, ax

   ;->Quitar el up de MP<-
   mov  eax, cr0
   and  eax, 0xfffffffe
   mov  cr0, eax

   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   inc edi
   mov byte [edi], 0x5a
   ;--> TEST 02 FIN<--
   
   mov edi, 0x300000
   mov byte [edi], 0xa5
   inc edi
   mov byte [edi], 0xa5
   inc edi
   mov byte [edi], 0xa5


..@fault_end:
   hlt
   jmp ..@fault_end


GDT:
NULL_SEL    equ $-GDT
   dq 0x0
DS_SEL      equ $-GDT
   dw 0xffff 
   dw 0x0000
   db 0x00
   db 0x92
   db 0xcf
   db 0
GDT_LENGTH  equ $-GDT

gdtr:
   dw GDT_LENGTH-1
   dd GDT
;///////////////////////////////////////////////////////////////////////////////
;                   Funciones para habilitar el A20 Gate
;///////////////////////////////////////////////////////////////////////////////
%define     PORT_A_8042    0x60        ;Puerto A de E/S del 8042
%define     CTRL_PORT_8042 0x64        ;Puerto de Estado del 8042
%define     KEYB_DIS       0xAD        ;Deshabilita teclado con Command Byte
%define     KEYB_EN        0xAE        ;Habilita teclado con Command Byte
%define     READ_OUT_8042  0xD0        ;Copia en 0x60 el estado de OUT
%define     WRITE_OUT_8042 0xD1        ;Escribe en OUT lo almacenado en 0x60

USE16
;------------------------------------------------------------------------------
;| Título: A20_Enable_No_Stack                                                |
;| Versión:       1.0                     Fecha:   26/02/2018                 |
;| Autor:         ChristiaN               Modelo:  IA-32 (16bits)             |
;| ------------------------------------------------------------------------   |
;| Descripción:                                                               |
;|    Habilita la puerta A20 sin utilizacion de la pila.                      |
;|    Referencia https://wiki.osdev.org/A20_Line                              |
;| ------------------------------------------------------------------------   |
;| Recibe:                                                                    |
;|    Nada                                                                    |
;|                                                                            |
;| Retorna:                                                                   |
;|    Nada                                                                    |
;| ------------------------------------------------------------------------   |
;| Revisiones:                                                                |
;|    1.0 | 26/02/2018 | ChristiaN | Original                                 |
;------------------------------------------------------------------------------
A20_Enable_No_Stack:

   xor ax, ax
   ;Deshabilita el teclado
   mov di, .8042_kbrd_dis
   jmp .empty_8042_in
   .8042_kbrd_dis:
   mov al, KEYB_DIS
   out CTRL_PORT_8042, al
 
   ;Lee la salida
   mov di, .8042_read_out
   jmp .empty_8042_in
   .8042_read_out:
   mov al, READ_OUT_8042
   out CTRL_PORT_8042, al
   
   .empty_8042_out:  
;      in al, CTRL_PORT_8042      ; Lee port de estado del 8042 hasta que el
;      test al, 00000001b         ; buffer de salida este vacio
;      jne .empty_8042_out

   xor bx, bx   
   in al, PORT_A_8042
   mov bx, ax

   ;Modifica el valor del A20
   mov di, .8042_write_out
   jmp .empty_8042_in
   .8042_write_out:
   mov al, WRITE_OUT_8042
   out CTRL_PORT_8042, al

   mov di, .8042_set_a20
   jmp .empty_8042_in
   .8042_set_a20:
   mov ax, bx
   or ax, 00000010b              ; Habilita el bit A20
   out PORT_A_8042, al

   ;Habilita el teclado
   mov di, .8042_kbrd_en
   jmp .empty_8042_in
   .8042_kbrd_en:
   mov al, KEYB_EN
   out CTRL_PORT_8042, al

   mov di, .a20_enable_no_stack_exit
   .empty_8042_in:  
;      in al, CTRL_PORT_8042      ; Lee port de estado del 8042 hasta que el
;      test al, 00000010b         ; buffer de entrada este vacio
;      jne .empty_8042_in
      jmp di

   .a20_enable_no_stack_exit:

jmp A20_Enable_No_Stack_return
   
CODE_LENGTH equ ($-start16)

times (RESET_VECTOR - ROM_START - CODE_LENGTH) nop
reset_vector:
   cli
   cld
   jmp start16
align 16
