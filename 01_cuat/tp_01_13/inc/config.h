; ------------------------------------------------------------------------------
;  \file    config.h   
;  \version 01.00
;  \brief   Archivo de configuracion, aplica con directivas de precompilador
;
;  \author  Agustin Diaz Antuna
;  \date    29/05/2018
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------

; Configura el comportamiento del ISR32
   %define A_COUNTER       0
   %define A_SCHEDULER     1

      %define ISR32_IS        A_SCHEDULER


; Configura el comportamiento del ISR32
   %define OFF    0
   %define ON     1

      %define KEYBOARD_IS     ON



