; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL tabla_digitos_count
GLOBAL timer_count
GLOBAL last_count
GLOBAL page_addr_phy

GLOBAL current_task
GLOBAL next_task
GLOBAL count_t1
GLOBAL count_t2


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .bss nobits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss
; ------------------------------------------------------------------------------

; Variables del keyboard
   tabla_digitos_count:
      resd 1


; Variables de timer, contadores hasta 10 (500ms)
   timer_count:
      resb 1         ; Reservo 1 byte

   last_count:
      resb 1         ; Reservo 1 byte


; Variables de scheduler
   current_task:
      resb 1         ; Reservo 1 byte

   next_task:
      resb 1         ; Reservo 1 byte

   count_t1:
      resb 1


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .datos progbits alloc noexec write


; ------------------------------------------------------------------------------
; --- bss
; ------------------------------------------------------------------------------

; Variables de ISR14: PAGE FAULT
   page_addr_phy:
      dd 0x10000000    ; Reservo 1 dword


; Variables de scheduler
   count_t2:
      db 0x01          ; Reservo 1 byte







