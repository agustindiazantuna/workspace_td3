/** 
   \file    task2.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 2

   \author  Agustin Diaz Antuna
   \date    29/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */
// Tabla digitos
extern qword tabla_digitos[1000];
extern dword tabla_digitos_count;


// Variables local de la tarea 2
dword suma_tabla_digitos_t2 __T2_BSS__ = 0;


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__T2_TEXT__ void task02(void)
{

   #if (TASK02_IS == DUMMY)
      
      while(1)
      {
         BOCHS_STOP
         asm("mov %0,%%eax"::"r"(0xFFFF0002));
         __asm__("hlt");
      }

   #elif (TASK02_IS == ON)

      qword i = 0;
   //   dword *pointer = 0;
   //   dword lectura = 0;


      while(1)
      {
   /*      
         if(suma_tabla_digitos_t2 == 0x4000)
         {
   //         BOCHS_STOP
            asm("mov %0,%%eax"::"r"(0xFFFF0002));
            asm("mov %0,%%eax"::"r"(suma_tabla_digitos_t2));
         }
   */

         suma_tabla_digitos_t2 = 0;

         for(i = 0; i < tabla_digitos_count; i++)
            suma_tabla_digitos_t2 += tabla_digitos[i];


         asm("hlt");

   /*
         // Trato de leer y escribir la posicion de memoria indicada por la suma
         if(suma_tabla_digitos_t2 < 0x20000000 && suma_tabla_digitos_t2 != 0) // 512MB
         {
            pointer = (dword *) suma_tabla_digitos_t2;
            lectura = *pointer;
            *pointer = lectura;
         }
   */
   }

#endif

}







