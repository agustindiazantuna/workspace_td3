; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------
USE32


%if 0

; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define k_table_attr_w        0x0003
%define k_page_attr_w         0x0003


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN ptree_entry

EXTERN _PAG_TABLE_START
EXTERN __isq_vma
EXTERN __systables_vma
EXTERN __kernel_vma_st
EXTERN _TABLA_DIGITOS_START
EXTERN __DATA_vma_st
EXTERN _STACK_START
EXTERN _STACK_PAGE1
EXTERN _STACK_PAGE2
EXTERN init_pagetables_ret


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL init_pagetables


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
SECTION .start32 progbits


; ------------------------------------------------------------------------------
; --- init_pagetables
; ------------------------------------------------------------------------------
init_pagetables:


; __UTILS32__ void ptree_entry(dword ptree_base, dword addr_phy,
;                              dword addr_lin, word table_attr, 
;                              word page_attr)
;
;
; ------------CODIGO---------------
; --------------------------------- page_attr   0x0003
;     8  G        0 : Global
;
;     7  PAT      0 : 
;     6  D        0 : Dirty, no modificada
;     5  A        0 : Accedida
;     4  PCD      0 : Cache disable
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel) - 1 : Usuario
;     1  R/W      1 : 0 R - 1 W
;     0  P        1 : Presente
; --------------------------------- table_attr  0x0003
;     7  PS       0 : 4KB
;     6  IGN      0 : Ignorado
;     5  A        0 : Accedida
;     4  PCD      0
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel)
;     1  R/W      1 : 0 R - 1 W
;     0  P        1
;
;
; ------------DATA---------------
; --------------------------------- page_attr   0x0103
;     8  G        0 : Global
;
;     7  PAT      0
;     6  D        0 : Dirty, no modificada
;     5  A        0 : Accedida
;     4  PCD      0
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel)
;     1  R/W      1 : 0 R - 1 W
;     0  P        1
; --------------------------------- table_attr  0x0003
;     7  PS       0 : 4KB
;     6  IGN      0 : Ignorado
;     5  A        0 : Accedida
;     4  PCD      0
;
;     3  PWT      0
;     2  U/S      0 : Supervisor (kernel)
;     1  R/W      1 : 0 R - 1 W
;     0  P        1

   .load_isr:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __isq_vma            ;  arg3  -> addr_lin
      push __isq_vma            ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_systables:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __systables_vma      ;  arg3  -> addr_lin
      push __systables_vma      ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_pagetables:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push _PAG_TABLE_START     ;  arg3  -> addr_lin
      push _PAG_TABLE_START     ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_kernel:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __kernel_vma_st      ;  arg3  -> addr_lin
      push __kernel_vma_st      ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_tabla_digitos:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push _TABLA_DIGITOS_START ;  arg3  -> addr_lin
      push _TABLA_DIGITOS_START ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_data:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push __DATA_vma_st        ;  arg3  -> addr_lin
      push __DATA_vma_st        ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

   .load_stack:
      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push _STACK_PAGE1         ;  arg3  -> addr_lin
      push _STACK_PAGE1         ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave

      push ebp
      mov ebp, esp
      push k_table_attr_w        ;  arg5  -> page_attr
      push k_table_attr_w        ;  arg4  -> table_attr
      push _STACK_PAGE2         ;  arg3  -> addr_lin
      push _STACK_PAGE2         ;  arg2  -> addr_phy
      push _PAG_TABLE_START     ;  arg1  -> ptree_base
      call ptree_entry
      leave


   jmp init_pagetables_ret


%endif







