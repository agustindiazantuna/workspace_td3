/** 
   \file    paging.c   
   \version 01.00
   \brief   Funciones utilizadas en kernel.asm

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */
#define table_attr_k_w        0x0003
#define page_attr_k_w         0x0003

#define table_attr_u_w        0x0007      // deberia ser 0x0007
#define page_attr_u_w         0x0007

/*
BOCHS_STOP
   asm("mov %0,%%eax"::"r"(variable));
   asm("nop"::);
BOCHS_STOP
*/
/*if(i==150)
{
   BOCHS_STOP
      asm("mov %0,%%eax"::"r"(pointer_DTP));

}*/


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */

// physical - kernel
extern dword __isq_lin;
extern dword __systables_lin;
extern dword _PAG_TABLE_t0_START;
extern dword _PAG_TABLE_t1_START;
extern dword _PAG_TABLE_t2_START;
extern dword __kernel_lin;
extern dword _TABLA_DIGITOS_START;
extern dword __DATA_lin;
extern dword _STACK_START;


// physical - user
extern dword __TEXT_t0_phy;
extern dword _BSS_t0_START_phy;
extern dword _DATA_t0_START_phy;

extern dword __TEXT_t2_phy;
extern dword _BSS_t2_START_phy;
extern dword _DATA_t2_START_phy;

extern dword __TEXT_t1_phy;
extern dword _BSS_t1_START_phy;
extern dword _DATA_t1_START_phy;

extern dword _STACK_t0_nivel0_START_phy;
extern dword _STACK_t2_nivel0_START_phy;
extern dword _STACK_t1_nivel0_START_phy;

extern dword _STACK_t0_START_phy;
extern dword _STACK_t2_START_phy;
extern dword _STACK_t1_START_phy;


// lineal - user
extern dword __TEXT_t0_lin;
extern dword _BSS_t0_START_lin;
extern dword _DATA_t0_START_lin;

extern dword __TEXT_t2_lin;
extern dword _BSS_t2_START_lin;
extern dword _DATA_t2_START_lin;

extern dword __TEXT_t1_lin;
extern dword _BSS_t1_START_lin;
extern dword _DATA_t1_START_lin;

extern dword _STACK_t0_nivel0_START_lin;
extern dword _STACK_t2_nivel0_START_lin;
extern dword _STACK_t1_nivel0_START_lin;

extern dword _STACK_t0_START_lin;
extern dword _STACK_t2_START_lin;
extern dword _STACK_t1_START_lin;




/* ------------------------------------------------------------------------------
 * --- ptree_entry
 * ------------------------------------------------------------------------------
 *
 ********************************************************************************
 *  Paginacion sin PAE
 *
 *  +-----------------+  ptree_base
 *  |PDe0             |
 *  +-----------------+  Solo una tabla de directorios de 1024 entredas de 4B c/u
 *  |PDe1             |  Indice de la tabla index_DTP
 *  +-----------------+  Entrada de la tabla la guardo en el mismo puntero
 *  |...              |
 *  +-----------------+
 *  |PDe1023          |
 *  +-----------------+  ptree_base+4kB+0*4kB
 *  |PTe0.0           |
 *  +-----------------+
 *  |PTe0.1           |
 *  +-----------------+
 *  |...              |
 *  +-----------------+
 *  |PTe0.1023        |
 *  +-----------------+  ptree_base+4kB+x*4kB
 *  |PTex.0           |
 *  +-----------------+  1024 tablas de paginas de 1024 entredas de 4B c/u
 *  |PTex.1           |  Indice de la tabla ptree_pt
 *  +-----------------+  Entrada de la tabla ptree_pte
 *  |...              |
 *  +-----------------+
 *  |PTex.1023        |
 *  +-----------------+  ptree_base+4kB+1023*4kB
 *  |PTe1023.0        |
 *  +-----------------+
 *  |PDe1023.1        |
 *  +-----------------+
 *  |...              |
 *  +-----------------+
 *  |PDe1023.1023     |
 *  +-----------------+
 *
 ********************************************************************************
 *
 ********************************************************************************
 *
 * @fn         ptree_entry
 *
 * @brief      Funcion de carga de datos en DTP y TP.
 *
 * @param [in] ptree_base     direccion de comienzo de la DTP
 *
 * @param [in] addr_phy       direccion fisica a cargar
 *
 * @param [in] addr_lin       direccion lineal a cargar
 *
 * @param [in] table_attr     atributos de la TP
 *
 * @param [in] page_attr      atributos de la pagina
 *
 * @return     0              exito
 *             1              DTP_entry con distintos atributos
 *             2              TP_entry con distintos atributos
 *
 ********************************************************************************/
// byte i __BSS__ = 0;
__UTILS32_RAM__ byte ptree_entry(dword ptree_base, dword addr_phy, dword addr_lin, \
                              word table_attr, word page_attr)
{
   // Indice DTP - indice TP
   dword index_DTP = 0, index_TP = 0;
   // Address TP - address P
   dword addr_TP = 0, addr_P = 0;
   // Entry DTP - entry TP
   dword entry_DTP = 0, entry_TP = 0;


   // Obtengo indices
   index_DTP = ((addr_lin >> 22) & 0x000003FF);
   index_TP = ((addr_lin >> 12) & 0x000003FF);


   /* ---------------------------- Agrego entrada a la DTP ---------------------------- */
      // Apunto al comienzo de DTP
      dword * pointer_DTP = (dword *) ptree_base;

      // Apunto a la posicion pedida
      pointer_DTP += index_DTP;

      // Obtengo direccion de la TP
      addr_TP = (((ptree_base + (0x1000 * (index_DTP + 1))) >> 12) & 0x000FFFFF);

      // Armo entry_DTP
      entry_DTP = (addr_TP << 12) + (table_attr & 0x0FFF);


      // Chequeo estado de DTP_entry
      if(*pointer_DTP == 0x00)
      {
         // Ingreso entrada a la DTP
         *pointer_DTP = entry_DTP;
      }
      else if((*pointer_DTP & ~(0x00000020)) != entry_DTP)  // Enmascaro el bit Accedido
      {
         // DTP_entry con distintos atributos
         return 1;
      }


   /* ---------------------------- Agrego entrada a la TP ---------------------------- */
      // Apunto al comienzo de la TP
      dword * pointer_TP = (dword *) (addr_TP << 12);

      // Apunto a la posicion pedida
      pointer_TP += index_TP;

      // Obtengo direccion de la P
      addr_P = ((addr_phy >> 12) & 0x000FFFFF);

      // Armo entry_TP
      entry_TP = (addr_P << 12) + (page_attr & 0x0FFF);


      // Chequeo estado de TP_entry
      if(*pointer_TP == 0x00)
      {
         // Ingreso entrada a la TP
         *pointer_TP = entry_TP;
      }
      else if((*pointer_TP & ~(0x00000020)) != entry_TP)  // Enmascaro el bit Accedido
      {
         // TP_entry con distintos atributos
         return 2;
      }


      return 0;
}


/* ------------------------------------------------------------------------------
 * --- init_pagetables_c
 * ------------------------------------------------------------------------------
 *
 ********************************************************************************
 *
 * @fn         init_pagetables_c
 *
 * @brief      Funcion de carga rapida de cada TPE
 *
 * @param      void
 *
 * @return     void
 *
 ********************************************************************************/
__UTILS32__ void init_pagetables_c(void)
{
   /*
   // physical - kernel
   extern dword __isq_lin;
   extern dword __systables_lin;
   extern dword _PAG_TABLE_t0_START;
   extern dword _PAG_TABLE_t1_START;
   extern dword _PAG_TABLE_t2_START;
   extern dword __kernel_lin;
   extern dword _TABLA_DIGITOS_START;
   extern dword __DATA_lin;
   extern dword _STACK_START;


   // physical - user
   extern dword __TEXT_t0_phy;
   extern dword _BSS_t0_START_phy;
   extern dword _DATA_t0_START_phy;

   extern dword __TEXT_t2_phy;
   extern dword _BSS_t2_START_phy;
   extern dword _DATA_t2_START_phy;

   extern dword __TEXT_t1_phy;
   extern dword _BSS_t1_START_phy;
   extern dword _DATA_t1_START_phy;

   extern dword _STACK_t0_nivel0_START_phy;
   extern dword _STACK_t2_nivel0_START_phy;
   extern dword _STACK_t1_nivel0_START_phy;

   extern dword _STACK_t0_START_phy;
   extern dword _STACK_t2_START_phy;
   extern dword _STACK_t1_START_phy;


   // lineal - user
   extern dword __TEXT_t0_lin;
   extern dword _BSS_t0_START_lin;
   extern dword _DATA_t0_START_lin;

   extern dword __TEXT_t2_lin;
   extern dword _BSS_t2_START_lin;
   extern dword _DATA_t2_START_lin;

   extern dword __TEXT_t1_lin;
   extern dword _BSS_t1_START_lin;
   extern dword _DATA_t1_START_lin;

   extern dword _STACK_t0_nivel0_START_lin;
   extern dword _STACK_t2_nivel0_START_lin;
   extern dword _STACK_t1_nivel0_START_lin;

   extern dword _STACK_t0_START_lin;
   extern dword _STACK_t2_START_lin;
   extern dword _STACK_t1_START_lin;

   // Si hay errores
   
   if(newpage != 0)
   {
      BOCHS_STOP
      newpage = 0;
   }
   */

   byte i = 0;
   byte newpage = 0;


   /************************************ kernel pages ************************************/
   // phy_addr    kernel   write
   dword phy_addr_k_w [9] = \
      {
         (dword) &__isq_lin,              (dword) &__systables_lin,     \
         (dword) &_PAG_TABLE_t0_START,    (dword) &_PAG_TABLE_t1_START, \
         (dword) &_PAG_TABLE_t2_START,    (dword) &__kernel_lin,        \
         (dword) &_TABLA_DIGITOS_START,   (dword) &__DATA_lin,          \
         (dword) &_STACK_START
      };


   for(i = 0; i < 9; i++)
   {
      newpage = ptree_entry((dword) &_PAG_TABLE_t0_START, (dword) phy_addr_k_w[i], \
                           (dword) phy_addr_k_w[i], table_attr_u_w, page_attr_k_w);


      newpage = ptree_entry((dword) &_PAG_TABLE_t1_START, (dword) phy_addr_k_w[i], \
                           (dword) phy_addr_k_w[i], table_attr_u_w, page_attr_k_w);


      newpage = ptree_entry((dword) &_PAG_TABLE_t2_START, (dword) phy_addr_k_w[i], \
                           (dword) phy_addr_k_w[i], table_attr_u_w, page_attr_k_w);
   }   


   // phy_addr    kernel   read
//   dword phy_addr_k_r [0] = {0};


   /************************************ table pages ************************************/
   for(i = 1; i < 129; i++)
   {
      newpage = ptree_entry((dword) &_PAG_TABLE_t0_START, (dword) &_PAG_TABLE_t0_START + (i * 0x1000), \
                           (dword) &_PAG_TABLE_t0_START + (i * 0x1000), table_attr_u_w, page_attr_k_w);

      newpage = ptree_entry((dword) &_PAG_TABLE_t1_START, (dword) &_PAG_TABLE_t1_START + (i * 0x1000), \
                           (dword) &_PAG_TABLE_t1_START + (i * 0x1000), table_attr_u_w, page_attr_k_w);

      newpage = ptree_entry((dword) &_PAG_TABLE_t2_START, (dword) &_PAG_TABLE_t2_START + (i * 0x1000), \
                           (dword) &_PAG_TABLE_t2_START + (i * 0x1000), table_attr_u_w, page_attr_k_w);
   }   




   /************************************ user pages ************************************/
   // phy_addr    user     write
   dword phy_addr_u_w_t0 [5] = \
      {
         (dword) &__TEXT_t0_phy,                (dword) &_BSS_t0_START_phy,            \
         (dword) &_DATA_t0_START_phy,           (dword) &_STACK_t0_nivel0_START_phy,   \
         (dword) &_STACK_t0_START_phy                                                  \
      };

   dword phy_addr_u_w_t1 [5] = \
      {
         (dword) &__TEXT_t1_phy,                (dword) &_BSS_t1_START_phy,            \
         (dword) &_DATA_t1_START_phy,           (dword) &_STACK_t1_nivel0_START_phy,   \
         (dword) &_STACK_t1_START_phy                                                  \
      };

   dword phy_addr_u_w_t2 [5] = \
      {
         (dword) &__TEXT_t2_phy,                (dword) &_BSS_t2_START_phy,            \
         (dword) &_DATA_t2_START_phy,           (dword) &_STACK_t2_nivel0_START_phy,   \
         (dword) &_STACK_t2_START_phy                                                  \
      };


   dword lin_addr_u_w_t0 [5] = \
      {
         (dword) &__TEXT_t0_lin,                (dword) &_BSS_t0_START_lin,            \
         (dword) &_DATA_t0_START_lin,           (dword) &_STACK_t0_nivel0_START_lin,   \
         (dword) &_STACK_t0_START_lin                                                  \
      };

   dword lin_addr_u_w_t1 [5] = \
      {
         (dword) &__TEXT_t1_lin,                (dword) &_BSS_t1_START_lin,            \
         (dword) &_DATA_t1_START_lin,           (dword) &_STACK_t1_nivel0_START_lin,   \
         (dword) &_STACK_t1_START_lin                                                  \
      };

   dword lin_addr_u_w_t2 [5] = \
      {
         (dword) &__TEXT_t2_lin,                (dword) &_BSS_t2_START_lin,            \
         (dword) &_DATA_t2_START_lin,           (dword) &_STACK_t2_nivel0_START_lin,   \
         (dword) &_STACK_t2_START_lin                                                  \
      };


   for(i = 0; i < 5; i++)
   {
      newpage = ptree_entry((dword) &_PAG_TABLE_t0_START, (dword) phy_addr_u_w_t0[i], \
                           (dword) lin_addr_u_w_t0[i], table_attr_u_w, page_attr_u_w);

      newpage = ptree_entry((dword) &_PAG_TABLE_t1_START, (dword) phy_addr_u_w_t1[i], \
                           (dword) lin_addr_u_w_t1[i], table_attr_u_w, page_attr_u_w);

      newpage = ptree_entry((dword) &_PAG_TABLE_t2_START, (dword) phy_addr_u_w_t2[i], \
                           (dword) lin_addr_u_w_t2[i], table_attr_u_w, page_attr_u_w);
   }


   // phy_addr    user     read
//   dword phy_addr_u_r [0] = {0};

   // dummy
   newpage = newpage;
}







