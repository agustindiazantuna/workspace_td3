/* Keyboard type: 
 * 
 * Scan code set 1
 * 
 * Source:     https://wiki.osdev.org/PS/2_Keyboard
 *
 */

#define KEY_1_PRESSED               0x02
#define KEY_2_PRESSED               0x03
#define KEY_3_PRESSED               0x04
#define KEY_4_PRESSED               0x05
#define KEY_5_PRESSED               0x06
#define KEY_6_PRESSED               0x07
#define KEY_7_PRESSED               0x08
#define KEY_8_PRESSED               0x09
#define KEY_9_PRESSED               0x0A
#define KEY_0_PRESSED               0x0B

#define KEY_Q_PRESSED               0x10
#define KEY_W_PRESSED               0x11
#define KEY_E_PRESSED               0x12
#define KEY_R_PRESSED               0x13
#define KEY_T_PRESSED               0x14
#define KEY_Y_PRESSED               0x15
#define KEY_U_PRESSED               0x16
#define KEY_I_PRESSED               0x17
#define KEY_O_PRESSED               0x18
#define KEY_P_PRESSED               0x19

#define KEY_A_PRESSED               0x1E
#define KEY_S_PRESSED               0x1F
#define KEY_D_PRESSED               0x20
#define KEY_F_PRESSED               0x21
#define KEY_G_PRESSED               0x22
#define KEY_H_PRESSED               0x23
#define KEY_J_PRESSED               0x24
#define KEY_K_PRESSED               0x25
#define KEY_L_PRESSED               0x26

#define KEY_Z_PRESSED               0x2C
#define KEY_X_PRESSED               0x2D
#define KEY_C_PRESSED               0x2E
#define KEY_V_PRESSED               0x3F
#define KEY_B_PRESSED               0x30
#define KEY_N_PRESSED               0x31
#define KEY_M_PRESSED               0x32

#define KEY_ENTER_PRESSED           0x1C