/** 
   \file    paging.c   
   \version 01.00
   \brief   Funciones utilizadas en kernel.asm

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */
#define table_attr_k_w        0x0003
#define page_attr_k_w         0x0003

#define table_attr_u_w        0x0003
#define page_attr_u_w         0x0003
/*
BOCHS_STOP
   asm("mov %0,%%eax"::"r"(variable));
BOCHS_STOP
*/


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */
extern dword ___isq_vma;
extern dword ___systables_vma;
extern dword __PAG_TABLE_START;
extern dword ___kernel_vma_st;
extern dword __TABLA_DIGITOS_START;

extern dword ___TEXT_task_1_vma_st;
extern dword __BSS_task_1_START;
extern dword __DATA_task_1_START;

extern dword __DATA_START;
extern dword __STACK_PAGE1;
extern dword __STACK_PAGE2;

extern dword __STACK_task_1_PAGE1;
extern dword __STACK_task_1_PAGE2;


/* ------------------------------------------------------------------------------
 * --- ptree_entry
 * ------------------------------------------------------------------------------
 *
 ********************************************************************************
 *  Paginacion sin PAE
 *
 *  +-----------------+  ptree_base
 *  |PDe0             |
 *  +-----------------+  Solo una tabla de directorios de 1024 entredas de 4B c/u
 *  |PDe1             |  Indice de la tabla index_DTP
 *  +-----------------+  Entrada de la tabla la guardo en el mismo puntero
 *  |...              |
 *  +-----------------+
 *  |PDe1023          |
 *  +-----------------+  ptree_base+4kB+0*4kB
 *  |PTe0.0           |
 *  +-----------------+
 *  |PTe0.1           |
 *  +-----------------+
 *  |...              |
 *  +-----------------+
 *  |PTe0.1023        |
 *  +-----------------+  ptree_base+4kB+x*4kB
 *  |PTex.0           |
 *  +-----------------+  1024 tablas de paginas de 1024 entredas de 4B c/u
 *  |PTex.1           |  Indice de la tabla ptree_pt
 *  +-----------------+  Entrada de la tabla ptree_pte
 *  |...              |
 *  +-----------------+
 *  |PTex.1023        |
 *  +-----------------+  ptree_base+4kB+1023*4kB
 *  |PTe1023.0        |
 *  +-----------------+
 *  |PDe1023.1        |
 *  +-----------------+
 *  |...              |
 *  +-----------------+
 *  |PDe1023.1023     |
 *  +-----------------+
 *
 ********************************************************************************
 *
 ********************************************************************************
 *
 * @fn         ptree_entry
 *
 * @brief      Funcion de carga de datos en DTP y TP.
 *
 * @param [in] ptree_base     direccion de comienzo de la DTP
 *
 * @param [in] addr_phy       direccion fisica a cargar
 *
 * @param [in] addr_lin       direccion lineal a cargar
 *
 * @param [in] table_attr     atributos de la TP
 *
 * @param [in] page_attr      atributos de la pagina
 *
 * @return     void
 *
 ********************************************************************************/
__UTILS32__ void ptree_entry(dword ptree_base, dword addr_phy, dword addr_lin, \
                              word table_attr, word page_attr)
{
   // Indice DTP - indice TP
   dword index_DTP = 0, index_TP = 0;
   // Address TP - address P
   dword addr_TP = 0, addr_P = 0;
   // Entry DTP - entry TP
   dword entry_DTP = 0, entry_TP = 0;


   // Obtengo indices
   index_DTP = ((addr_lin >> 22) & 0x000003FF);
   index_TP = ((addr_lin >> 12) & 0x000003FF);


   /* ---------------------------- Agrego entrada a la DTP ---------------------------- */
      // Apunto al comienzo de DTP
      dword * pointer_DTP = (dword *) ptree_base;

      // Apunto a la posicion pedida
      pointer_DTP += index_DTP;

      // Obtengo direccion de la TP
      addr_TP = (((ptree_base + (0x1000 * (index_DTP + 1))) >> 12) & 0x000FFFFF);

      // Armo entry_DTP
      entry_DTP = (addr_TP << 12) + (table_attr & 0x0FFF);

      // Ingreso entrada a la DTP
      *pointer_DTP = entry_DTP;

   /* ---------------------------- Agrego entrada a la TP ---------------------------- */
      // Apunto al comienzo de la TP
      dword * pointer_TP = (dword *) (addr_TP << 12);

      // Apunto a la posicion pedida
      pointer_TP += index_TP;

      // Obtengo direccion de la P
      addr_P = ((addr_phy >> 12) & 0x000FFFFF);

      // Armo entry_TP
      entry_TP = (addr_P << 12) + (page_attr & 0x0FFF);

      // Ingreso entrada a la TP
      *pointer_TP = entry_TP;
}


/* ------------------------------------------------------------------------------
 * --- init_pagetables_c
 * ------------------------------------------------------------------------------
 *
 ********************************************************************************
 *
 * @fn         init_pagetables_c
 *
 * @brief      Funcion de carga rapida de cada TPE
 *
 * @param      void
 *
 * @return     void
 *
 ********************************************************************************/
__UTILS32__ void init_pagetables_c(void)
{
   /*
   extern dword ___isq_vma;
   extern dword ___systables_vma;
   extern dword __PAG_TABLE_START;
   extern dword ___kernel_vma_st;
   extern dword __TABLA_DIGITOS_START;

      extern dword ___TEXT_task_1_vma_st;
      extern dword __BSS_task_1_START;
      extern dword __DATA_task_1_START;

   extern dword __DATA_START;
   extern dword __STACK_PAGE1;
   extern dword __STACK_PAGE2;

      extern dword __STACK_task_1_PAGE1;
      extern dword __STACK_task_1_PAGE2;
   */

   byte i = 0;

   // phy_addr    kernel   write
   dword phy_addr_k_w [8] = \
      {
         (dword) &___isq_vma,                (dword) &___systables_vma,       \
         (dword) &__PAG_TABLE_START,         (dword) &___kernel_vma_st,       \
         (dword) &__TABLA_DIGITOS_START,     (dword) &__DATA_START,           \
         (dword) &__STACK_PAGE1,             (dword) &__STACK_PAGE2           \
      };

   for(i = 0; i < 8; i++)
   {
      ptree_entry((dword) &__PAG_TABLE_START, (dword) phy_addr_k_w[i], phy_addr_k_w[i], \
                  table_attr_k_w, page_attr_k_w);
   }   


   // phy_addr    kernel   read
//   dword phy_addr_k_r [0] = {0};

   /* Dejo preparada la forma de cargar paginas de usuario para el caso futuro en el
   * que tengamos dos niveles de privilegio.
   */
   // phy_addr    user     write
   dword phy_addr_u_w [5] = \
      {
         (dword) &___TEXT_task_1_vma_st,     (dword) &__BSS_task_1_START,     \
         (dword) &__DATA_task_1_START,       (dword) &__STACK_task_1_PAGE1,   \
         (dword) &__STACK_task_1_PAGE2                                        \
      };

   for(i = 0; i < 5; i++)
   {
      ptree_entry((dword) &__PAG_TABLE_START, (dword) phy_addr_u_w[i], phy_addr_u_w[i], \
                  table_attr_u_w, page_attr_u_w);
   }


   // phy_addr    user     read
//   dword phy_addr_u_r [0] = {0};


}







