/** 
   \file    task1.c   
   \version 01.00
   \brief   Funciones correspondientes a la Tarea 1

   \author  Agustin Diaz Antuna
   \date    15/05/2018
*/

/* ------------------------------------------------------------------------------
 * --- include
 * ------------------------------------------------------------------------------
 */
#include "../inc/sys_types.h"


/* ------------------------------------------------------------------------------
 * --- define
 * ------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------
 * --- global
 * ------------------------------------------------------------------------------
 */
// Tabla digitos
extern qword tabla_digitos[1000];
extern qword suma_tabla_digitos;


// Contador de ingresos de la tabla de digitos
extern dword tabla_digitos_count;


/* ------------------------------------------------------------------------------
 * --- add_tabla_digitos
 * ------------------------------------------------------------------------------
 */
__TASK_1_TEXT__ void add_tabla_digitos(void)
{
   qword i = 0;

   suma_tabla_digitos = 0;

   for(i = 0; i < tabla_digitos_count; i++)
      suma_tabla_digitos += tabla_digitos[i];

   return;
}