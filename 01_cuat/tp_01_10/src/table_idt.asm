; ------------------------------------------------------------------------------
; --- Directivas
; ------------------------------------------------------------------------------


; ------------------------------------------------------------------------------
; --- define
; ------------------------------------------------------------------------------
%define __BASE    CS_SEL_0


; ------------------------------------------------------------------------------
; --- EXTERN
; ------------------------------------------------------------------------------
EXTERN __LIMITE_H
EXTERN __LIMITE_L
EXTERN CS_SEL_0

EXTERN isr0_handler_DE
EXTERN isr2_handler_NMI
EXTERN isr3_handler_BP
EXTERN isr4_handler_OF
EXTERN isr5_handler_BR
EXTERN isr6_handler_UD
EXTERN isr7_handler_NM
EXTERN isr8_handler_DF
EXTERN isr10_handler_TS
EXTERN isr11_handler_NP
EXTERN isr12_handler_SS
EXTERN isr13_handler_GP
EXTERN isr14_handler_PF
EXTERN isr16_handler_MF
EXTERN isr17_handler_AC
EXTERN isr18_handler_MC
EXTERN isr19_handler_XF
EXTERN isr32_handler_PIT
EXTERN isr33_handler_KEYBOARD


; ------------------------------------------------------------------------------
; --- GLOBAL
; ------------------------------------------------------------------------------
GLOBAL idt_to_idtr


; ------------------------------------------------------------------------------
; --- SECTION
; ------------------------------------------------------------------------------
; progbits : datos definidos
; nobits : datos no inicializados
SECTION .idt_table progbits alloc noexec nowrite


; ------------------------------------------------------------------------------
; --- struc
; ------------------------------------------------------------------------------
; Propiedades [7:0]
;   P     [7]     Present (1: presente en memoria)
;   DPL   [6:5]   Descriptor Privilege Level (00: kernel - 11: user)
;   S     [4]     Storage Segment (0: sistema - 1: codigo o datos)
;   T     [3:0]   Type:
;     0101    32 bit task gate
;     0110    16-bit interrupt gate
;     0111    16-bit trap gate
;     1110    32-bit interrupt gate
;     1111    32-bit trap gate


struc   taskgate_t                ; Selector taskgate_t
        .reserv3:       resw 1    ; Reservado
        .seg_tss:       resw 1    ; Selector de segmento tss
        .reserv2:       resb 1    ; Reservado
        .prop:          resb 1    ; Propiedades     1000_0101 0x85
        .reserv1:       resw 1    ; Reservado
endstruc

struc   intgate_t                 ; Selector intgate_t
        .lim_00_15:     resw 1    ; Limite del segmento 00-15
        .seg:           resw 1    ; Selector de segmento
        .reserv:        resb 1    ; Reservado
        .prop:          resb 1    ; Propiedades     1000_1110 0x8E
        .lim_16_31:     resw 1    ; Limite del segmento 16-31
endstruc

struc   trapgate_t                ; Selector trapgate_t
        .lim_00_15:     resw 1    ; Limite del segmento 00-15
        .seg:           resw 1    ; Selector de segmento
        .reserv:        resb 1    ; Reservado
        .prop:          resb 1    ; Propiedades     1000_1111 0x8F
        .lim_16_31:     resw 1    ; Limite del segmento 16-31
endstruc


; ------------------------------------------------------------------------------
; --- IDT
; ------------------------------------------------------------------------------
;IDT:
;  ISR0_DE    
;  ISR1           Reservado
;  ISR2_NMI
;  ISR3_BP
;  ISR4_OF
;  ISR5_BR
;  ISR6_UD
;  ISR7_NM
;  ISR8_DF
;  ISR9           Reservado
;  ISR10_TS
;  ISR11_NP
;  ISR12_SS
;  ISR13_GP
;  ISR14_PF
;  ISR15          Reservado
;  ISR16_MF
;  ISR17_AC
;  ISR18_MC
;  ISR19_XF
;  ISR20_31       Reservado
;  ISR32_PIT
;  ISR33_KEYBOARD
;  ISR34_47       Usuario


IDT:
  ISR0_DE         equ $-IDT                     ; Define 0
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr0_handler_DE
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR1            equ $-IDT                     ; Define 8
    dq 0x0

  ISR2_NMI        equ $-IDT                     ; Define 16
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr2_handler_NMI
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR3_BP         equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr3_handler_BP
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR4_OF         equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr4_handler_OF
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR5_BR         equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr5_handler_BR
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR6_UD         equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr6_handler_UD
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR7_NM         equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr7_handler_NM
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR8_DF         equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr8_handler_DF
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR9            equ $-IDT
      dq 0x0

  ISR10_TS        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr10_handler_TS
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR11_NP        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr11_handler_NP
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR12_SS        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr12_handler_SS
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR13_GP        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr13_handler_GP
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR14_PF        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr14_handler_PF
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR15           equ $-IDT
      dq 0x0

  ISR16_MF        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr16_handler_MF
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR17_AC        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr17_handler_AC
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR18_MC        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr18_handler_MC
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR19_XF        equ $-IDT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr19_handler_XF
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8F
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR20_31        equ $-IDT        ; Reservado
      times 12 dq 0x0

  ISR32_PIT       equ $-IDT        ; PIT
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr32_handler_PIT
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8E
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR33_KEYBOARD  equ $-IDT        ; KEYBOARD
    istruc trapgate_t
      at trapgate_t.lim_00_15,     dw isr33_handler_KEYBOARD
      at trapgate_t.seg,           dw __BASE
      at trapgate_t.reserv,        db 0x00
      at trapgate_t.prop,          db 0x8E
      at trapgate_t.lim_16_31,     dw __LIMITE_H
    iend

  ISR34_47        equ $-IDT        ; Disponible para el usuario
      times 15 dq 0x0

  IDT_LENGTH  equ $-IDT

idt_to_idtr:
  dw IDT_LENGTH-1            ; Tamaño de la tabla en reg 16 bits
  dd IDT                     ; Dirección relativa en reg 32 bits







